#!/usr/bin/env python
import rospy
import tf
from tf import transformations
from nav_msgs.msg import Odometry
from geometry_msgs.msg import PoseStamped, PointStamped, Twist
from geodesy import utm
from geographiclib.geodesic import Geodesic
import time
from simple_pid import PID
import math
from nav_msgs.msg import Path

pid = PID(0.055, 0.00, 0.01, setpoint=1)
position_x = 0.0
position_y = 0.0
dist_precision_ = 0.4
yaw_precision = math.radians(2.0)
yaw_ = 0.0
turn_angle = 0.0


def generate_path(poses, path_, angle):
    q = transformations.quaternion_from_euler(0, 0, angle)
    for x, y, z in poses:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    return path_


def get_odometry(data):
    global position_x, position_y, yaw_
    position_x = data.pose.pose.position.x
    position_y = data.pose.pose.position.y
    quaternion = (data.pose.pose.orientation.x,
                  data.pose.pose.orientation.y,
                  data.pose.pose.orientation.z,
                  data.pose.pose.orientation.w)
    yaw_ = transformations.euler_from_quaternion(quaternion)[2]


def path_shifter(latlons_, angle):
    geod = Geodesic.WGS84
    gps_points = []
    for lat, lon, dir in latlons_:
        point_ = geod.Direct(lat, lon, angle, 0.5)
        gps_points.append([point_['lat2'], point_['lon2'], dir])
    return gps_points


def get_latlons(text):
    data = text.read()
    coordinates = data.split("\n")
    latlon = []
    count = 1
    for data in coordinates:
        if len(data.split(" ")) > 1:
            latlon.append([float(data.split(" ")[0]), float(data.split(" ")[1]), float(data.split(" ")[2])])
            turn_angle = float(data.split(" ")[3])
    update_latlons = []
    count = 0
    for a, b, c in latlon:
        if round(c, 2) == 0.01:
            update_latlons.append([a, b, round(c, 2)])
            count = 0
        elif round(c, 2) != 0.01 and count == 0:
            update_latlons.append([a, b, round(c, 2)])
            update_latlons.append([a, b, -round(c, 2)])
            count = count + 1
    shifted_latlons = path_shifter(update_latlons, turn_angle)
    return shifted_latlons


def convert_gps_to_map_points(gps_points_):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon, dir in gps_points_:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.point.z = dir
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y, point.point.z])
        # print(point.point.x, point.point.y, point.point.z)
    return map_point


def calculate_error_yaw(required, present):
    # if 0.0 >= required >= -90.0 and 0.0 < present <= 90.0 \
    #         or (0.0 <= required <= 180.0 and 0.0 <= present <= 180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 >= present >= -180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 <= present <= 180.0)\
    #         or (0.0 <= required <= 180.0 and 0.0 >= present >= -180.0):
    if required < -90.0 and present > 90.0:
        return 360.0 + (required - present)
    elif required >= 90.0 and present <= -90.0:
        return (required - present) - 360.0
    else:
        return required - present


def own_planner(pointx, pointy, vel):
    global yaw_, yaw_precision, state_
    twist_msg = Twist()
    err_pos = math.sqrt(pow(pointy - position_y, 2) +
                        pow(pointx - position_x, 2))
    rate = rospy.Rate(20)
    while err_pos > dist_precision_:
        desired_yaw = math.degrees(math.atan2(pointy - position_y,
                                              pointx - position_x))
        err_pos = math.sqrt(pow(pointy - position_y, 2) +
                            pow(pointx - position_x, 2))

        err_yaw = calculate_error_yaw(desired_yaw, math.degrees(yaw_))
        twist_msg.linear.x = 0.3
        pid.setpoint = 0
        pid.output_limits = (-0.6, 0.6)
        output = pid(err_yaw)
        print(err_pos, err_yaw, output)
        twist_msg.angular.z = -output
        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)


def timer_based_planner(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(150):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(110):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


def timer_based_planner_r(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(130):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(100):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


if __name__ == '__main__':
    rospy.init_node("controller_recorded", anonymous=True)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    rospy.Subscriber("/odometry/filtered_map", Odometry, get_odometry)
    f = open("/home/xmachines/gps_points_sim.txt", "r")
    pub_vis = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    path_ = Path()
    latlons = get_latlons(f)
    for a, b, c in latlons:
        print(a, b, c)
    map_points = convert_gps_to_map_points(latlons)
    path__ = generate_path(map_points, path_, 180.0)
    for x in range(50):
        pub_vis.publish(path__)
        time.sleep(0.1)
    while not rospy.is_shutdown():
        for x, y, z in map_points:
            if z == 0.01:
                rospy.loginfo("using own planner")
                own_planner(x, y, cmd_vel)
            if z == 0.02:
                rospy.loginfo("using move_base_flex")
                # move_base_flex_planner(mbf_ep_ac, x.pose.position.x, x.pose.position.y)
            if z == -0.03:
                rospy.loginfo("using time based planner")
                timer_based_planner(cmd_vel, -1)
            if z == 0.03:
                rospy.loginfo("using time based planner")
                timer_based_planner(cmd_vel, 1)
            if z == -0.04:
                rospy.loginfo("using time based planner -0.04")
                timer_based_planner_r(cmd_vel, -1)
            if z == 0.04:
                rospy.loginfo("using time based planner 0.04")
                timer_based_planner_r(cmd_vel, 1)
        break


