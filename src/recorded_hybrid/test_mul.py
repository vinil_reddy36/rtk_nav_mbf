#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64MultiArray

a = [[4.083627887070179, 4.018461863801349, 0.01, 5.0], [5.0832149004563689, 4.0236803991138004, 0.01, 5.0], [4.083627887070179, 4.018461863801349, 0.01, 5.0], [1.0,2.0,3.0,4.0]]

rospy.init_node("hybrid_path_creator_v2_1", anonymous=True)
data_pub = rospy.Publisher("/global_plan/plan", Float64MultiArray, queue_size=10)
DATA = Float64MultiArray()

for b in a:
    print(b)
    DATA.data.append(b)
print(DATA)
for i in range(10000):
    data_pub.publish(DATA)