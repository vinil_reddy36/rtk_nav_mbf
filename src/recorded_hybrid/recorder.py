#!/usr/bin/env python

import rospy
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32MultiArray, Float32
import time

current_lat = 0.0
current_lon = 0.0
vel_x = 0.0
forward_angle = 0.0
turn_angle = 0.0
turn = 0.0
type_ = 0.0


def get_global_plan_data(data_):
    global turn_angle, forward_angle, turn
    turn_angle = data_.data[0]
    forward_angle = data_.data[1]
    turn = data_.data[2]


def get_type(data):
    global type_
    type_ = data.data


def get_cmd_vel(data):
    global vel_x
    vel_x = data.linear.x


def get_gps_data(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
    rospy.init_node("recorder", anonymous=True)
    rospy.Subscriber("/fix", NavSatFix, get_gps_data)
    rospy.Subscriber("/cmd_vel", Twist, get_cmd_vel)
    rospy.Subscriber("/type", Float32, get_type)
    rospy.Subscriber("/global_plan/data", Float32MultiArray, get_global_plan_data)
    f = open("/home/xmachines/gps_points_sim.txt", "a")
    try:
        rospy.loginfo("recording gps points")
        while not rospy.is_shutdown():
            if vel_x > 0.1 or vel_x < -0.1:
                f.write(str(current_lat) + " " + str(current_lon) + " " + str(type_) + " " + str(turn_angle) + "\n")
                print(type_)
            time.sleep(0.5)
    except KeyboardInterrupt:
        rospy.loginfo("Done recording, file saved")
        f.close()