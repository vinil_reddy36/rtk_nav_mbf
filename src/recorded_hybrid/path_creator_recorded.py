#!/usr/bin/env python

import rospy
import actionlib
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, PointStamped
from mbf_msgs.msg import ExePathGoal
import mbf_msgs.msg as mbf_msgs
import tf.listener
from tf import transformations
from geodesy import utm
import math


def get_latlons(text):
    data = text.read()
    coordinates = data.split("\n")
    latlon = []
    count = 1
    for data in coordinates:
        if len(data.split(" ")) > 1:
            latlon.append([float(data.split(" ")[0]), float(data.split(" ")[1])])
    return latlon


def generate_path(poses, angle):
    path_ = Path()
    q = transformations.quaternion_from_euler(0, 0, math.radians(angle))
    count = 0
    for x, y in poses:
        count = count + 1
        if count < len(poses):
            pose = PoseStamped()
            pose.header.frame_id = "map"
            pose.pose.position.x = x
            pose.pose.position.y = y
            pose.pose.orientation.z = 0
            pose.pose.orientation.w = 1
            pose.header.stamp = rospy.Time.now()
            path_.header.frame_id = "map"
            path_.header.stamp = rospy.Time.now()
            path_.poses.append(pose)
        else:
            pose = PoseStamped()
            pose.header.frame_id = "map"
            pose.pose.position.x = x
            pose.pose.position.y = y
            pose.pose.orientation.x = q[0]
            pose.pose.orientation.y = q[1]
            pose.pose.orientation.z = q[2]
            pose.pose.orientation.w = q[3]
            pose.header.stamp = rospy.Time.now()
            path_.header.frame_id = "map"
            path_.header.stamp = rospy.Time.now()
            path_.poses.append(pose)
    return path_


def convert_gps_to_map_points(gps_points):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon in gps_points:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y])
    return map_point


if __name__ == '__main__':
    rospy.init_node("recorded_nav")
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    mbf_ep_ac.wait_for_server(rospy.Duration(10))
    f = open("/home/vinil/gps_points.txt", "r")
    latlons = get_latlons(f)
    target_path_ = ExePathGoal()
    map_points = convert_gps_to_map_points(latlons)
    path = generate_path(map_points, 0.0)
    pub.publish(path)
    target_path_.path = path
    mbf_ep_ac.send_goal(target_path_)
    mbf_ep_ac.wait_for_result()

