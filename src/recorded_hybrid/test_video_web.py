#!/usr/bin/env python


import time
import rospy
import cv2
from cv_bridge import CvBridge
from sensor_msgs.msg import Image

video = cv2.VideoCapture(0)

if __name__=='__main__':
    rospy.init_node("web_cam", anonymous=True)
    image = rospy.Publisher("/xmachines/weeding_camera", Image, queue_size=1)
    bridge = CvBridge()
    while not rospy.is_shutdown():
        _,frame = video.read()
        frame = cv2.resize(frame, (100,100))
        stream_msg = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
        image.publish(stream_msg)
        time.sleep(0.001)
