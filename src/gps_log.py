#!/usr/bin/env python

import time
import rospy
import datetime
import os
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import Twist

date = str(datetime.datetime.now()).split()[0]
time_ = str(datetime.datetime.now()).split()[1]
directory = "" + date + "/" + time_ + ""
parent_dir = "/home/xmachines/gps/"
path = os.path.join(parent_dir, directory)
if os.path.isdir(path):
    pass
else:
    os.makedirs(path)
f = open(path + time_ + "gps_points.txt", "a")
current_lat = 0.0
current_lon = 0.0
vel_x = 0.0
ang_z = 0.0


def get_vel(data):
    global vel_x , ang_z
    vel_x = data.linear.x
    ang_z = data.angular.z


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
    rospy.Subscriber("/fix", NavSatFix, get_current_lat_lon)
    rospy.Subscriber("/cmd_vel", Twist, get_vel)
    while not rospy.is_shutdown():
        if vel_x > 0.1 or ang_z > 0.1 or vel_x < -0.1 or ang_z < -0.1:
            f.write(str(current_lat) + " " + str(current_lon) + "\n")
    time.sleep(0.01)