<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "X100";
$conn = mysqli_connect($GLOBALS['servername'] , $GLOBALS['username'], $GLOBALS['password'], $GLOBALS['dbname'], 5522);

function get_data_($data_){
    if (!$GLOBALS['conn']) {
    die("Connection failed: " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM a20230327";
    $result = mysqli_query($GLOBALS['conn'], $sql);

    if (mysqli_num_rows($result) > 0) {
    // output data of each row

      while($row = mysqli_fetch_assoc($result)){
        echo "" . $row["lat"] . " " .$row["lon"] . " " . $row["z"] . "/n";
      }
    } 
    
    else {
      echo "0 results";
    }
    #mysqli_close($GLOBALS['conn']);
}

function update_pos($index){
  if (!$GLOBALS['conn']) {
    die("Connection failed: " . mysqli_connect_error());
    }

    $sql = "SELECT * FROM ac20230327 WHERE status=1";
    $result = mysqli_query($GLOBALS['conn'], $sql);

    if (mysqli_num_rows($result) > 0) {
    // output data of each row
      while($row = mysqli_fetch_assoc($result)){
        echo "" . $row["lat"] . " " .$row["lon"] . " " . $row["z"] . " " . $row["id"] ."/n";
      }
    } 
    
    else {
      echo "0 results";
    }
}


if (isset($_POST['update_pos'])){
   echo update_pos($_POST['update_pos']);
}

if (isset($_POST['get_data'])) {
    echo get_data_($_POST['get_data']);
}

?>
