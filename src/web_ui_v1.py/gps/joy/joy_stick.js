var x = 0.0;
var y = 0.0;
var scale_x = 0.3;
var scale_z = 1.0;


function clicked(button_pressed) {
    switch (button_pressed) {
        case "up":
            x = +scale_x;
            console.log(x);
            break;
        case "down":
            x = -scale_x;
            break;
        case "left":
            y = +scale_z;
            break;
        case "right":
            y = -scale_z;
            break;
    }
}
function unclicked(button_pressed) {
    x = 0.0;
    y = 0.0;
}

var slider_x = document.getElementById("slider_x");
var output_x = document.getElementById("value_x");
output_x.innerHTML = slider_x.value;

slider_x.oninput = function () {
    output_x.innerHTML = this.value;
    scale_x = this.value;
}

var slider_z = document.getElementById("slider_z");
var output_z = document.getElementById("value_z");
output_z.innerHTML = slider_z.value;

slider_z.oninput = function () {
    output_z.innerHTML = this.value;
    scale_z = this.value;
}


function send_unpause(value){
    var unpause = new ROSLIB.Topic({
        ros : ros,
        name : '/pause',
        messageType : 'std_msgs/Bool'
      });
      
     var unpause_state = new ROSLIB.Message({
      data : value
      });
      unpause.publish(unpause_state);
}


function send_web_ui_cmd_vel(){
    var coordinates = [x, y];
    var user_input = new ROSLIB.Topic({
        ros : ros,
        name : '/web_ui_cmd_vel',
        messageType : 'std_msgs/Float32MultiArray'
    });
    
    var state = new ROSLIB.Message({
        data : coordinates
    });
    user_input.publish(state);
}

var interval_count = 0
var is_robot_paused_listener = new ROSLIB.Topic({
    ros : ros,
    name : '/robot_is_paused',
    messageType : 'std_msgs/Bool'
  });
  intervel_loop = "";
  is_robot_paused_listener.subscribe(function(message) {
    if(message.data){
        if(interval_count == 0){
            intervel_loop = setInterval(send_web_ui_cmd_vel, 100);
            interval_count = interval_count + 1
        }
        
    }
    else{
        clearInterval(intervel_loop);
        interval_count = 0;
    }
  });

  var debug_listener = new ROSLIB.Topic({
    ros : ros,
    name : '/debug_msgs',
    messageType : 'xmachines_msgs/debug'
  });
  debug_listener.subscribe(function(message) {
    document.getElementById("gps").value = message.gps.msg;
    document.getElementById("t265").value = message.t265.msg;
    document.getElementById("magnetometer").value = message.magnetometer.msg;
    document.getElementById("navigation").value = message.navigation.msg;
    document.getElementById("ultrasonic").value = message.ultrasonic.msg;
  });
