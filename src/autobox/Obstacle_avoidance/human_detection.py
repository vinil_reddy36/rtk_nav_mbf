
import cv2
import pyrealsense2 as rs

# Initialize the RealSense camera
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
pipeline.start(config)

# Initialize the human detection model
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

# Main loop
while True:
    # Wait for a new frame from the camera
    frames = pipeline.wait_for_frames()
    color_frame = frames.get_color_frame()
    if not color_frame:
        continue
    
    # Convert the color image to OpenCV format
    color_image = np.asanyarray(color_frame.get_data())
    
    # Detect humans in the image
    (rects, weights) = hog.detectMultiScale(color_image, winStride=(8, 8))
    
    # Draw rectangles around the detected humans
    for (x, y, w, h) in rects:
        cv2.rectangle(color_image, (x, y), (x + w, y + h), (0, 0, 255), 2)
    
    # Display the image
    cv2.imshow('Human Detection', color_image)
    
    # Wait for a key press
    if cv2.waitKey(1) == ord('q'):
        break

# Clean up
pipeline.stop()
cv2.destroyAllWindows()
