#!/usr/bin/python

import cv2
import pyrealsense2 as rs
import numpy as np
from cv_bridge import CvBridge, CvBridgeError
import rospy
from sensor_msgs.msg import Image

# Initialize the RealSense camera
pipeline = rs.pipeline()
config = rs.config()
config.enable_device('920312070806')
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
pipeline.start(config)


if __name__=='__main__':
    rospy.init_node("camera_publisher", anonymous=True)
    image = rospy.Publisher("xmachines/camera", Image, queue_size=1)
    bridge = CvBridge()
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        # Wait for a new frame from the camera
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        if not color_frame:
            continue
        
        # Convert the color image to OpenCV format
        frame = np.asanyarray(color_frame.get_data())
        frame = cv2.resize(frame, (640, 480))
        stream_msg = bridge.cv2_to_imgmsg(frame, encoding="bgr8")
        image.publish(stream_msg)
        rate.sleep()
        
pipeline.stop()