#!/usr/bin/env python
# stable version, tested on field.
# global path creator with time based approach for multi row, working both left and right initial conditions. checked on simulation

# gps points:
# the first gps point should be taken jst few steps ahead of the robot,
# this point determines the forward angle, and length of the row is specified

# in time based planner approach: the robot rotates till 90 degree angle( in previous approach it excuted based on time)
# in this path creator extra information is send in a topic called /global_path/data.
import time
import rospy
from std_msgs.msg import Float32MultiArray
from geodesy import utm
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, PointStamped
import tf.listener
from tf import transformations
from sensor_msgs.msg import NavSatFix
from geographiclib.geodesic import Geodesic
import datetime
import pymysql as mysql


current_lat = 0.0
current_lon = 0.0
number_of_rows = 4
first_turn = "left"
arc_length = 4.0
row_gap = 1.0
length_of_row = 5.0

#B = (17.5192567, 78.2735518)
#C = (17.5192567, 78.2735452)  # left
B = (17.5192601, 78.2736114)
C = (17.5192619, 78.2736034)

# C = (49.9000393, 8.9000139) # right

date = str(datetime.datetime.now()).split()[0]
date = date.split('-')
date = ''.join(date)

# C = (49.9000393, 8.9000139) # right

mydb = mysql.connect(
        host="localhost",
        user="root",
        password="root",
        database="X100"
    )
mycursor = mydb.cursor()

def init_database():
    a = mycursor.execute("SHOW TABLES LIKE 'a" + date + "'")
    if a:
        print("table already exsists")
        mycursor.execute("DROP table a" + date + "")
        mycursor.execute("DROP table ac" + date + "")
        mycursor.execute("CREATE TABLE a" + date + ""
                                                   "(timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,"
                                                   "id int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                                   "lat DECIMAL(16, 13),"
                                                   "lon DECIMAL(16, 13),"
                                                   "z DECIMAL(16, 13))")
        mycursor.execute("CREATE TABLE ac" + date + ""
                                                   "(timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,"
                                                   "id int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                                   "lat DECIMAL(16, 13),"
                                                   "lon DECIMAL(16, 13),"
                                                   "z DECIMAL(16, 13), status INT DEFAULT 0)")
    else:
        mycursor.execute("CREATE TABLE a" + date + ""
                                                   "(timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,"
                                                   "id int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                                   "lat DECIMAL(16, 13),"
                                                   "lon DECIMAL(16, 13),"
                                                   "z DECIMAL(16, 13))")
        mycursor.execute("CREATE TABLE ac" + date + ""
                                                   "(timestamp timestamp NULL DEFAULT CURRENT_TIMESTAMP,"
                                                   "id int NOT NULL AUTO_INCREMENT PRIMARY KEY,"
                                                   "lat DECIMAL(16, 13),"
                                                   "lon DECIMAL(16, 13),"
                                                   "z DECIMAL(16, 13), status INT DEFAULT 0)")


def update_to_mysql(lat_lons):
    # for x in lat_lons:
    #     sql = "INSERT INTO a" + date + " (lat, lon, z) VALUES ('{}','{}','{}')".format(x[0], x[1], x[2])
    #     val = (x[0], x[1], x[2])
    #     #print(type(x[0]))
    #     mycursor.execute(sql)
    #     mydb.commit()
    #     time.sleep(0.1)
    sql = "INSERT INTO a" + date + " (lat, lon, z) VALUES (%s, %s, %s) "
    val = (lat_lons)
    mycursor.executemany(sql, val)
    mydb.commit()
    sql = "INSERT INTO ac" + date + " (lat, lon, z) VALUES (%s, %s, %s) "
    val = (lat_lons)
    mycursor.executemany(sql, val)
    mydb.commit()






def generate_path(poses, path_, angle):
    q = transformations.quaternion_from_euler(0, 0, angle)
    for x, y, z in poses:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    return path_


def convert_gps_to_map_points(gps_points_):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon, dir in gps_points_:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.point.z = dir
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y, point.point.z])
        # print(point.point.x, point.point.y, point.point.z)
    return map_point


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


def straight(pointAlat, pointAlon, angle, distance_, spacing):
    geod = Geodesic.WGS84
    gps_points_ = []
    for steps in range(int(distance_ / spacing)):
        point_ = geod.Direct(pointAlat, pointAlon, angle, (spacing * (steps + 1)))
        gps_points_.append([point_['lat2'], point_['lon2'], 0.01])
    end_point_lat, end_point_lon = point_['lat2'], point_['lon2']
    return gps_points_, end_point_lat, end_point_lon


def backward_curve_l(mid_lat, mid_lon, intersect_lat_, intersect_lon_, previous_row_end_lat, previous_row_end_lon,
                     turn_angle_, arc_length_, _forward_angle, row_space, angles, state):
    geod = Geodesic.WGS84
    backward_arc_center = geod.Direct(mid_lat, mid_lon, _forward_angle, arc_length_ + arc_length_)
    gps_points_ = []
    for x in angles:
        point = geod.Direct(backward_arc_center['lat2'],
                            backward_arc_center['lon2'],
                            x, arc_length_)
        if x == (turn_angle_ - 180.0 + 0.0) or x == (turn_angle_ + 180 - 0.0):
            gps_points_.append([point['lat2'], point['lon2'], state])
    # gps_points_.append([intersect_lat_, intersect_lon_, 0.01])
    next_row_point = geod.Direct(previous_row_end_lat, previous_row_end_lon, turn_angle_, row_space)
    return gps_points_, next_row_point['lat2'], next_row_point['lon2']


def forward_curve_l(row_end_lat_, row_end_lon_, arc_length_, turn_angle_, forward_angle_, row_space):
    geod_ = Geodesic.WGS84
    forward_arc_center = geod_.Direct(row_end_lat_, row_end_lon_, turn_angle_, arc_length_)
    gps_points_ = []
    back_curve_angles = ((turn_angle_ - 180.0 + 45.0), (turn_angle_ - 180.0 + 0.0))
    for angle_ in ((turn_angle_ + 180.0 - 45.0), (turn_angle_ + 180.0 - 90.0)):
        point = geod_.Direct(forward_arc_center['lat2'],
                             forward_arc_center['lon2'],
                             angle_,
                             arc_length_)
        if angle_ == (turn_angle_ + 180.0 - 45.0):
            intersect_lat, intersect_lon = point['lat2'], point['lon2']
        if angle_ == (turn_angle_ + 180.0 - 45.0):
            gps_points_.append([point['lat2'], point['lon2'], 0.03])
    back_curv_points, next_row_lat_, next_row_lon_ = backward_curve_l(forward_arc_center['lat2'],
                                                                      forward_arc_center['lon2'],
                                                                      intersect_lat, intersect_lon,
                                                                      row_end_lat_, row_end_lon_,
                                                                      turn_angle_, arc_length_,
                                                                      forward_angle_, row_space,
                                                                      back_curve_angles, -0.03)
    gps_points_.extend(back_curv_points)
    gps_points_.extend([[next_row_lat_, next_row_lon_, 0.01]])
    return gps_points_, next_row_lat_, next_row_lon_


def reverse_curve_l(row_end_lat_, row_end_lon_, arc_length_, turn_angle_, backward_angle_, row_space):
    geod_ = Geodesic.WGS84
    back_curve_angles = ((turn_angle_ + 180 - 45), (turn_angle_ + 180 - 0.0))
    backward_arc_center = geod_.Direct(row_end_lat_, row_end_lon_, turn_angle_, arc_length_)
    gps_points_ = []
    for angle_ in ((turn_angle_ + 180.0 + 45.0), (turn_angle_ + 180.0 + 90.0)):
        point = geod_.Direct(backward_arc_center['lat2'],
                             backward_arc_center['lon2'],
                             angle_,
                             arc_length_)
        if angle_ == (turn_angle_ + 180.0 + 45.0):
            intersect_lat, intersect_lon = point['lat2'], point['lon2']
        if angle_ == (turn_angle_ + 180.0 + 45.0):
            gps_points_.append([point['lat2'], point['lon2'], 0.04])
    back_curv_points, next_row_lat_, next_row_lon_ = backward_curve_l(backward_arc_center['lat2'],
                                                                      backward_arc_center['lon2'],
                                                                      intersect_lat, intersect_lon,
                                                                      row_end_lat_, row_end_lon_,
                                                                      turn_angle_, arc_length_,
                                                                      backward_angle_, row_space,
                                                                      back_curve_angles, -0.04)
    gps_points_.extend(back_curv_points)
    gps_points_.extend([[next_row_lat_, next_row_lon_, 0.01]])
    return gps_points_, next_row_lat_, next_row_lon_


def backward_curve_r(mid_lat, mid_lon, intersect_lat_, intersect_lon_, previous_row_end_lat, previous_row_end_lon,
                     turn_angle_, arc_length_, _forward_angle, row_space, angles, state):
    geod = Geodesic.WGS84
    backward_arc_center = geod.Direct(mid_lat, mid_lon, _forward_angle, arc_length_ + arc_length_)
    gps_points_ = []
    for x in angles:
        point = geod.Direct(backward_arc_center['lat2'],
                            backward_arc_center['lon2'],
                            x, arc_length_)
        if x == (turn_angle_ + 180.0 - 0.0) or x == (turn_angle_ - 180 + 0):
            gps_points_.append([point['lat2'], point['lon2'], state])
    # gps_points_.append([intersect_lat_, intersect_lon_, 0.01])
    next_row_point = geod.Direct(previous_row_end_lat, previous_row_end_lon, turn_angle_, row_space)
    return gps_points_, next_row_point['lat2'], next_row_point['lon2']


def forward_curve_r(row_end_lat_, row_end_lon_, arc_length_, turn_angle_, forward_angle_, row_space):
    geod_ = Geodesic.WGS84
    forward_arc_center = geod_.Direct(row_end_lat_, row_end_lon_, turn_angle_, arc_length_)
    gps_points_ = []
    back_curve_angles = ((turn_angle_ + 180.0 - 45.0), (turn_angle_ + 180.0 - 0.0))
    for angle_ in ((turn_angle_ - 180.0 + 45.0), (turn_angle_ - 180.0 + 90.0)):
        point = geod_.Direct(forward_arc_center['lat2'],
                             forward_arc_center['lon2'],
                             angle_,
                             arc_length_)
        if angle_ == (turn_angle_ - 180.0 + 45.0):
            intersect_lat, intersect_lon = point['lat2'], point['lon2']
        if angle_ == (turn_angle_ - 180.0 + 45.0):
            gps_points_.append([point['lat2'], point['lon2'], 0.04])
    back_curv_points, next_row_lat_, next_row_lon_ = backward_curve_r(forward_arc_center['lat2'],
                                                                      forward_arc_center['lon2'],
                                                                      intersect_lat, intersect_lon,
                                                                      row_end_lat_, row_end_lon_,
                                                                      turn_angle_, arc_length_,
                                                                      forward_angle_, row_space,
                                                                      back_curve_angles, -0.04)
    gps_points_.extend(back_curv_points)
    gps_points_.extend([[next_row_lat_, next_row_lon_, 0.01]])
    return gps_points_, next_row_lat_, next_row_lon_


def reverse_curve_r(row_end_lat_, row_end_lon_, arc_length_, turn_angle_, backward_angle_, row_space):
    geod_ = Geodesic.WGS84
    back_curve_angles = ((turn_angle_ - 180 + 45), (turn_angle_ - 180 + 0))
    backward_arc_center = geod_.Direct(row_end_lat_, row_end_lon_, turn_angle_, arc_length_)
    gps_points_ = []
    for angle_ in ((turn_angle_ + 180.0 - 45.0), (turn_angle_ + 180.0 - 90.0)):
        point = geod_.Direct(backward_arc_center['lat2'],
                             backward_arc_center['lon2'],
                             angle_,
                             arc_length_)
        if angle_ == (turn_angle_ + 180.0 - 45.0):
            intersect_lat, intersect_lon = point['lat2'], point['lon2']
        if angle_ == (turn_angle_ + 180.0 - 45.0):
            gps_points_.append([point['lat2'], point['lon2'], 0.03])
    back_curv_points, next_row_lat_, next_row_lon_ = backward_curve_r(backward_arc_center['lat2'],
                                                                      backward_arc_center['lon2'],
                                                                      intersect_lat, intersect_lon,
                                                                      row_end_lat_, row_end_lon_,
                                                                      turn_angle_, arc_length_,
                                                                      backward_angle_, row_space,
                                                                      back_curve_angles, -0.03)
    gps_points_.extend(back_curv_points)
    gps_points_.extend([[next_row_lat_, next_row_lon_, 0.01]])
    return gps_points_, next_row_lat_, next_row_lon_


rec_sub_done = False
def get_gps_points(data):
    global B, C, rec_sub_done
    if len(data.data) > 4:
        B[0] = data.data[0]
        B[1] = data.data[1]
        C[0] = data.data[2]
        C[1] = data.data[3]
        rec_sub_done = True


if __name__ == '__main__':
    rospy.init_node("hybrid_path_creator_v2_1", anonymous=True)
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    data_pub = rospy.Publisher("/global_plan/data", Float32MultiArray, queue_size=10)
    pub_vis = rospy.Publisher("/global_plan", Path, queue_size=10)
    rospy.Subscriber("/fix", NavSatFix, get_current_lat_lon)
    rospy.Subscriber("/recorded_gps_points", Float32MultiArray, get_gps_points)
    path_ = Path()
    done = False
    latlons = []
    got_first_reading = False
    count = 0
    row = 0
    DATA = Float32MultiArray()
    init_database()
    while not rospy.is_shutdown() and done == False:
        # print(current_lat, current_lon)
        if current_lat != 0.0 and rec_sub_done:
            geod = Geodesic.WGS84
            get_forward = geod.Inverse(current_lat, current_lon, B[0], B[1])
            get_turn = geod.Inverse(B[0], B[1], C[0], C[1])
            turn_angle = get_turn['azi1']
            forward_angle = get_forward['azi1']
            distance = length_of_row
            lat = current_lat
            lon = current_lon
            angle = forward_angle
            got_first_reading = True
            print(turn_angle, forward_angle)
        if got_first_reading:
            count = 0
            row = 0
            if first_turn == "left":
                while count < number_of_rows:
                    gps_points_forward, row_end_lat, row_end_lon = straight(lat, lon, angle, distance, spacing=0.5)
                    latlons.extend(gps_points_forward)
                    count = count + 1
                    if row == 0 and count < number_of_rows:
                        gps_points_curve, next_row_lat, next_row_lon = forward_curve_l(row_end_lat, row_end_lon,
                                                                                       arc_length,
                                                                                       turn_angle, angle,
                                                                                       row_gap)
                        latlons.extend(gps_points_curve)
                        lat, lon, angle = next_row_lat, next_row_lon, angle - 180.0
                        row = 1
                    elif row == 1 and count < number_of_rows:
                        gps_points_curve_b, next_row_lat, next_row_lon = reverse_curve_l(row_end_lat, row_end_lon,
                                                                                         arc_length, turn_angle,
                                                                                         angle,
                                                                                         row_gap)
                        latlons.extend(gps_points_curve_b)
                        lat, lon, angle = next_row_lat, next_row_lon, angle + 180.0
                        row = 0
                    # count = count + 1
                map_points = convert_gps_to_map_points(latlons)
                for x in map_points:
                    print(x)
                path__ = generate_path(map_points, path_, 180.0)
                for x in range(50):
                    pub_vis.publish(path__)
                    time.sleep(0.2)
                DATA.data = [turn_angle, forward_angle, 0.0]
                data_pub.publish(DATA)
                pub.publish(path__)
                update_to_mysql(latlons)
                done = True
            if first_turn == "right":
                while count < number_of_rows:
                    gps_points_forward, row_end_lat, row_end_lon = straight(lat, lon, angle, distance, spacing=0.5)
                    latlons.extend(gps_points_forward)
                    count = count + 1
                    if row == 0 and count < number_of_rows:
                        gps_points_curve, next_row_lat, next_row_lon = forward_curve_r(row_end_lat, row_end_lon,
                                                                                       arc_length,
                                                                                       turn_angle, angle,
                                                                                       row_gap)
                        latlons.extend(gps_points_curve)
                        lat, lon, angle = next_row_lat, next_row_lon, angle - 180.0
                        row = 1
                    elif row == 1 and count < number_of_rows:
                        gps_points_curve_b, next_row_lat, next_row_lon = reverse_curve_r(row_end_lat, row_end_lon,
                                                                                         arc_length, turn_angle,
                                                                                         angle,
                                                                                         row_gap)
                        latlons.extend(gps_points_curve_b)
                        lat, lon, angle = next_row_lat, next_row_lon, angle + 180.0
                        row = 0
                    # count = count + 1
                map_points = convert_gps_to_map_points(latlons)
                path__ = generate_path(map_points, path_, 180.0)
                for x in range(50):
                    pub_vis.publish(path__)
                    time.sleep(0.1)
                DATA.data = [turn_angle, forward_angle, 1.0]
                data_pub.publish(DATA)
                pub.publish(path__)
                update_to_mysql(latlons)
                done = True

        time.sleep(0.01)
