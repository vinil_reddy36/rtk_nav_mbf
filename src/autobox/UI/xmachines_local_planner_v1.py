#!/usr/bin/env python


import time
import rospy
from std_msgs.msg import Float32MultiArray, Bool
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Twist, PoseStamped
from mbf_msgs.msg import ExePathGoal
from tf import transformations
import math
from simple_pid import PID
from sensor_msgs.msg import NavSatFix, Imu
import datetime
import pymysql as mysql
from sensor_msgs.msg import Joy
from geographiclib.geodesic import Geodesic



# pid = PID(0.025, 0.001, 0.05, setpoint=1)
pid = PID(0.075, 0.0, 0.055, setpoint=1)
dist_precision_ = 0.4
path_ = Path()
yaw_precision = math.radians(2.0)
mag_yaw = 0.0
turn_angle = 0.0
forward_angle = 0.0
turn = 0.0
current_lat = 0.0
current_lon = 0.0
linear_x = 0.4
angular_x = 0.5


path_points_list = []
present_path_point_index = 0
pause = False
anamoly_pause = False
web_cmd_vel_x = 0.0
web_cmd_vel_z = 0.0 

date = str(datetime.datetime.now()).split()[0]
date = date.split('-')
date = ''.join(date)

mydb = mysql.connect(
        host="localhost",
        user="root",
        password="root",
        database="X100"
    )
mycursor = mydb.cursor()

def update_db(index):
    sql = "UPDATE ac" + date + " SET status = 1 WHERE id = " + str(index)+";" 
    mycursor.execute(sql)
    mydb.commit()
    print("status updated")

def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


def get_global_plan_data(data_):
    global turn_angle, forward_angle, turn
    turn_angle = data_.data[0]
    forward_angle = data_.data[1]
    turn = data_.data[2]


def calculate_error_yaw(required, present):
    # if 0.0 >= required >= -90.0 and 0.0 < present <= 90.0 \
    #         or (0.0 <= required <= 180.0 and 0.0 <= present <= 180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 >= present >= -180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 <= present <= 180.0)\
    #         or (0.0 <= required <= 180.0 and 0.0 >= present >= -180.0):
    if required < -90.0 and present > 90.0:
        return 360.0 + (required - present)
    elif required >= 90.0 and present <= -90.0:
        return (required - present) - 360.0
    else:
        return required - present


def own_planner(point, vel):
    global mag_yaw, yaw_precision, state_, current_lat, current_lon, present_path_point_index, pause, anamoly_pause
    twist_msg = Twist()
    geod = Geodesic.WGS84
    data = geod.Inverse(current_lat, current_lon, point[0], point[1])
    err_pos = data['s12']
    rate = rospy.Rate(20)
    while err_pos > dist_precision_:
        geod = Geodesic.WGS84
        data = geod.Inverse(current_lat, current_lon, point[0], point[1])
        desired_yaw = data['azi1']
        err_pos = data['s12']
        err_yaw = desired_yaw - mag_yaw
        if abs(err_yaw) > 35:
            pause = True
            rospy.loginfo("skipped previous point, pausing the robot")
            #present_path_point_index = present_path_point_index - 1
            break
        if not anamoly_pause:
            twist_msg.linear.x = 0.4
            pid.setpoint = 0
            pid.output_limits = (-0.55, 0.55)
            output = pid(err_yaw)
            print(err_pos, desired_yaw, math.degrees(mag_yaw), err_yaw, output)
            twist_msg.angular.z = -output
            vel.publish(twist_msg)
        else:
            twist_msg.linear.x = 0.0
            twist_msg.angular.z = 0.0
            vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)



def timer_based_planner(cmd_vel__, mul, angle):
    global mag_yaw, anamoly_pause
    angle = angle % 360
    #yaw = math.degrees(mag_yaw) % 360
    yaw = mag_yaw % 360
    print(angle, yaw)
    twist_msgs = Twist()
    if 0 < angle < 15:
        angle = 15
    if 345 < angle < 360:
        angle = 345
    if mul > 0:
        if angle < yaw:
            while angle < yaw :
                if not anamoly_pause:
                    yaw = math.degrees(mag_yaw) % 360
                    print(angle , yaw, "t_b_p mul>0 angle<yaw")
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
        else:
            while angle > yaw :
                if not anamoly_pause:
                    yaw = math.degrees(mag_yaw) % 360
                    print(angle , yaw, "t_b_p mul>0 angle>yaw")
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
    if mul < 0:
        if angle < yaw:
            while angle < yaw:
                if not anamoly_pause:
                    yaw = math.degrees(mag_yaw) % 360
                    print(angle, yaw, "t_b_p mul<0 angle<yaw")
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
        else:
            while angle > yaw:
                if not anamoly_pause:
                    yaw = math.degrees(mag_yaw) % 360
                    print(angle, yaw, "t_b_p mul<0 angle>yaw")
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


def timer_based_planner_r(cmd_vel__, mul, angle):
    global mag_yaw, anamoly_pause
    angle = angle % 360
    #yaw = math.degrees(mag_yaw) % 360
    yaw = mag_yaw % 360
    print(angle, yaw)
    if 0 < angle < 15:
        angle = 15
    if 345 < angle < 360:
        angle = 345
    twist_msgs = Twist()
    if mul > 0:
        if angle < yaw:
            while angle < yaw:
                if not anamoly_pause:
                    print(angle, yaw, "t_b_p_r mul>0 angle<yaw")
                    yaw = math.degrees(mag_yaw) % 360
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = -angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
        else:
            while angle > yaw or (angle+5) < yaw > (angle-5):
                if not anamoly_pause:
                    print(angle, yaw, "t_b_p_r mul>0 angle>yaw")
                    yaw = math.degrees(mag_yaw) % 360
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = -angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
    if mul < 0:
        if angle < yaw:
            while angle < yaw:
                if not anamoly_pause:
                    print(angle, yaw, "t_b_p_r mul<0 angle<yaw")
                    yaw = math.degrees(mag_yaw) % 360
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = -angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
        else:
            while angle > yaw:
                if not anamoly_pause:
                    print(angle, yaw, "t_b_p_r mul<0 angle>yaw")
                    yaw = math.degrees(mag_yaw) % 360
                    twist_msgs.linear.x = linear_x * mul
                    twist_msgs.angular.z = -angular_x
                    cmd_vel__.publish(twist_msgs)
                    time.sleep(0.1)
                else:
                    time.sleep(0.1)
                    continue
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


def get_magnetometer_yaw(data):
    global mag_yaw
    mag_yaw = -data.orientation.w


def get_pause_status(data):
    global pause
    pause = data.data


def get_anamoly_pause(data):
    global anamoly_pause
    anamoly_pause = data.data


def get_web_ui_cmd_vel(data):
    global web_cmd_vel_x, web_cmd_vel_z
    list_ = data.data
    web_cmd_vel_x = list_[0]
    web_cmd_vel_z = list_[1]


def get_gps_lat_lons(data):
    global gps_lat_lons
    gps_lat_lons = data.data


def unfaltten(list_):
    unfaltten_list = []
    points = []
    count = 0
    for point in list_:
        points.append(point)
        count = count + 1
        if count == 3:
            unfaltten_list.append(points)
            points = []
            count = 0
    return unfaltten_list



if __name__ == '__main__':
    rospy.init_node("xmachines_local_planner_v1", anonymous=True)
    rospy.Subscriber("/global_plan/data", Float32MultiArray, get_global_plan_data)
    rospy.Subscriber("/gps_lat_lons_path", Float32MultiArray, get_gps_lat_lons)
    rospy.Subscriber("/xmachines/imu", Imu, get_magnetometer_yaw)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    pest_cmd = rospy.Publisher("/xmachines/sprayer_on_state", Bool, queue_size=10)

    pest_state = Bool()
    intimate_web_ui_about_pause = rospy.Publisher("/robot_is_paused", Bool, queue_size=10)
    intimate_web_ui_about_pause_msg = Bool()
    rospy.Subscriber("/fix", NavSatFix, get_current_lat_lon)
    rospy.Subscriber("/pause", Bool, get_pause_status)
    rospy.Subscriber("/anamoly_pause", Bool, get_anamoly_pause)
    rospy.Subscriber("/web_ui_cmd_vel", Float32MultiArray, get_web_ui_cmd_vel)
    while not rospy.is_shutdown():
        if len(gps_lat_lons) > 1:
            path_points_list = unfaltten(gps_lat_lons)
            while present_path_point_index < len(path_points_list):
                if not (pause or anamoly_pause):
                    if path_points_list[present_path_point_index][2] == 0.01:
                        count = count + 1
                        rospy.loginfo("using own planner")
                        own_planner(path_points_list[present_path_point_index], cmd_vel)
                        if count == 2:
                            pest_state.data = True
                            pest_cmd.publish(pest_state)


                    if path_points_list[present_path_point_index][2] == -0.03:
                        rospy.loginfo("using time based planner")
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        if turn == 0.0:
                            timer_based_planner(cmd_vel, -1, forward_angle-180 + 25)
                        else:
                            timer_based_planner(cmd_vel, -1, forward_angle + 25)
                        count = 0

                    if path_points_list[present_path_point_index][2] == 0.03:
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        rospy.loginfo("using time based planner")
                        timer_based_planner(cmd_vel, 1, turn_angle)
                    

                    if path_points_list[present_path_point_index][2] == -0.04:
                        rospy.loginfo("using time based planner -0.04")
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        if turn == 0.0:
                            timer_based_planner_r(cmd_vel, -1, forward_angle - 15)
                        else:
                            timer_based_planner_r(cmd_vel, -1, forward_angle-180 - 15)
                    
                        count = 0
                    if path_points_list[present_path_point_index][2] == 0.04:
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        rospy.loginfo("using time based planner 0.04")
                        timer_based_planner_r(cmd_vel, 1, turn_angle)

                    update_db(present_path_point_index)
                    present_path_point_index = present_path_point_index + 1
                    intimate_web_ui_about_pause_msg.data = False
                    intimate_web_ui_about_pause.publish(intimate_web_ui_about_pause_msg)
                else:
                    if anamoly_pause:
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        twist = Twist()
                        twist.linear.x = 0.0
                        twist.angular.z = 0.0
                        cmd_vel.publish(twist)
                    if pause:
                        pest_state.data = False
                        pest_cmd.publish(pest_state)
                        intimate_web_ui_about_pause_msg.data = True
                        intimate_web_ui_about_pause.publish(intimate_web_ui_about_pause_msg)
                        twist = Twist()
                        twist.linear.x = web_cmd_vel_x
                        twist.angular.z = web_cmd_vel_z
                        cmd_vel.publish(twist)
                rate.sleep()
            break
        time.sleep(0.01)