#!/usr/bin/env python


import rospy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Twist
from simple_pid import PID
from nav_msgs.msg import Odometry
import math
from geographiclib.geodesic import Geodesic



current_lat = 0.0
current_lon = 0.0
desired_yaw = 50.0
yaw_ = 0.0
pid = PID(0.055, 0.00, 0.01, setpoint=1)
pid_gps = PID(0.055, 0.00, 0.01, setpoint=1)
x_distance = 0.0
goal_gps_angle = 0.0
present_heading_gps_angle = 0.0
A = []
B = []



def own_planner(goal_dis, vel):
    global yaw_, desired_yaw, x_distance, present_heading_gps_angle, goal_gps_angle, current_lat, current_lon
    twist_msg = Twist()
    rate = rospy.Rate(20)
    geod = Geodesic.WGS84
    goal_gps_angle = geod.Inverse(A[0], A[1], B[0], B[1])
    while goal_dis > x_distance: 
    	present_heading_gps_angle = geod.Inverse(current_lat, current_lon, B[0], B[1])
    	error_in_gps_heading_angle = goal_gps_angle - present_heading_gps_angle
    	if abs(error_in_gps_heading_angle) > 5:
    		twist.linear.x = 0.3
    		pid_gps.setpoint = 0
    		pid_gps.output_limits = (-0.6, 0.6)
    		gps_correction_output = pid_gps(error_in_gps_heading_angle)
    		print("in gps_heading_correction",  error_in_gps_heading_angle, present_heading_gps_angle, goal_gps_angle)
    		twist.angular.z = -gps_correction_output
    		vel.publish(twist_msg)
    	else:
	        err_yaw = (desired_yaw - yaw_)
	        twist_msg.linear.x = 0.3
	        pid.setpoint = 0
	        pid.output_limits = (-0.6, 0.6)
	        output = pid(err_yaw)
	        print(err_pos, desired_yaw, yaw_, err_yaw, output)
	        twist_msg.angular.z = -output
	        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)

def get_magnetometer_yaw(data):
	global yaw_
	yaw_ = data.orientation.w


def get_t265_odometry(data):
	global x_distance
	x_distance = data.pose.pose.position.x


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
	rospy.init_node("mag_nav_test")
	cmd_Vel_publisher = rospy.Publisher("/cmd_vel", Twist, queue_size=10)
	rospy.Subscriber("/xmachines/imu", Imu, get_magnetometer_yaw)
	rospy.Subscriber("/cam_1/odom/sample", Odometry, get_t265_odometry)
	rospy.Subscriber("/fix", NavSatFix, get_current_lat_lon)
	while not rospy.is_shutdown():
		
		own_planner(goal_distance, cmd_vel_publisher)
		break

