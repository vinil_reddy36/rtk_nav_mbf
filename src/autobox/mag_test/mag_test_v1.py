#!/usr/bin/env python


import rospy
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Twist
from simple_pid import PID
from nav_msgs.msg import Odometry
import math


desired_yaw = 50.0
yaw_ = 0.0
pid = PID(0.055, 0.00, 0.01, setpoint=1)
x_distance = 0.0
goal_distance = 20.0






def own_planner(goal_dis, vel):
    global yaw_, desired_yaw, x_distance
    twist_msg = Twist()
    rate = rospy.Rate(20)
    while goal_dis > x_distance: 
        err_yaw = (desired_yaw - yaw_)
        twist_msg.linear.x = 0.3
        pid.setpoint = 0
        pid.output_limits = (-0.6, 0.6)
        output = pid(err_yaw)
        print(err_pos, desired_yaw, math.degrees(yaw_), err_yaw, output)
        twist_msg.angular.z = -output
        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)

def get_magnetometer_yaw(data):
	global yaw_
	yaw_ = data.orientation.w


def get_t265_odometry(data):
	global x_distance
	x_distance = data.pose.pose.position.x




if __name__ == '__main__':
	rospy.init_node("mag_nav_test")
	cmd_Vel_publisher = rospy.Publisher("/cmd_vel", Twist, queue_size=10)
	rospy.Subscriber("/xmachines/imu", Imu, get_magnetometer_yaw)
	rospy.Subscriber("/cam_1/odom/sample", Odometry, get_t265_odometry)
	while not rospy.is_shutdown():
		own_planner(goal_distance, cmd_vel_publisher)
		break

