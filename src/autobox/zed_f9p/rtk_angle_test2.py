#!/usr/bin/python3.6


#this code produces heading angle given by ublox nav-relsponed msgs.
import math
import rospy
import serial
from nmea_msgs.msg import Sentence
from std_msgs.msg import Float32
import pynmea2
#import pandas
from datetime import datetime
from pyubx2 import UBXReader, exceptions, UBXMessage
from sensor_msgs.msg import NavSatFix
from geographiclib.geodesic import Geodesic
rover = serial.Serial('/dev/ttyACM0', 9600, timeout=None, parity='N', stopbits=1, bytesize=8, xonxoff=0, rtscts=0)
latFromNmea = 10.1
lonFromNmea = 10.1

def get_lat_lon(data):
    global latFromNmea, lonFromNmea
    latFromNmea = data.latitude
    lonFromNmea = data.longitude


def calculate_precision_angle(data):
    N = msg.relPosN# + (msg.relPosHPN * 1e-2)
    E = msg.relPosE# + (msg.relPosHPE * 1e-2)
    D = msg.relPosD# + (msg.relPosHPD * 1e-2)
    print(msg.relPosHeading)
    angle = msg.relPosHeading
    r = (N ** 2.0) + (E ** 2.0) + (D ** 2.0)
    r = math.sqrt(r)
    try:
        if E > 0:
            angle = 180 - math.degrees(math.acos(N / r)) + 180
            # angle = 180 + math.degrees(math.acos(N/r))
            #print("p")
            #print(angle)
        else:
            # angle = 180 - math.degrees(math.acos(N/r)))
            angle = -(180 - math.degrees(math.acos(N / r))) + 180
            #print("n")
            #print(angle)
    except ZeroDivisionError:
        print("not in RTK Mode")
        angle = 550.0
        pass
    return angle


rospy.init_node("nmea_sen_rtk_angle_publisher", anonymous=True)


fix_pub = rospy.Publisher("/fix_2", NavSatFix, queue_size=10)

count = 0
lat_ = 10.0
lon_ = 0.0
ang = 0.0
while not rospy.is_shutdown():
    ubr = UBXReader(rover)
    (raw_data, parsed_data) = ubr.read()
    fix_msg = NavSatFix()
    if len(raw_data) == 56 and raw_data[1] == 71:
        text = raw_data.decode("unicode_escape")
        check1 = text.split(',')
        if check1[0] == "$GNGLL":
            lat_ = parsed_data.lat
            lon_ = parsed_data.lon
            fix_msg.header.stamp = rospy.Time.now()
            fix_msg.header.frame_id = "gps"
            fix_msg.latitude = lat_
            fix_msg.longitude = lon_
            fix_pub.publish(fix_msg)
    if len(raw_data) == 72 and raw_data[1] == 98:
        msg = UBXReader.parse(raw_data)
		#print(msg)
        ang = calculate_precision_angle(msg)


