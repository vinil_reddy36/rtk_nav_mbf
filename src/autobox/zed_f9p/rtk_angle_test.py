#!/usr/bin/python

# this code subscribes to rover 1 and rover 2's lat & lon's. using them it produces heading angle

import rospy
from sensor_msgs.msg import NavSatFix
from geographiclib.geodesic import Geodesic

latFromNmea = 0.0
lonFromNmea = 0.0
latFromNmea_2 = 0.0
lonFromNmea_2 = 0.0

def get_lat_lon(data):
    global latFromNmea, lonFromNmea
    latFromNmea = data.latitude
    lonFromNmea = data.longitude

def get_lat_lon_2(data):
    global latFromNmea_2, lonFromNmea_2
    latFromNmea_2 = data.latitude
    lonFromNmea_2 = data.longitude


rospy.init_node("rtk_angle_publisher", anonymous=True)

rospy.Subscriber("/fix", NavSatFix, get_lat_lon)
rospy.Subscriber("/fix_2", NavSatFix, get_lat_lon_2)

rate = rospy.Rate(8)
while not rospy.is_shutdown():
    geod = Geodesic.WGS84
    data = geod.Inverse( latFromNmea_2,  lonFromNmea_2, latFromNmea, lonFromNmea)
    data2 = geod.Inverse( latFromNmea,  lonFromNmea, latFromNmea_2, lonFromNmea_2)
    print(data['azi1'], data2['azi1'])
    rate.sleep()

