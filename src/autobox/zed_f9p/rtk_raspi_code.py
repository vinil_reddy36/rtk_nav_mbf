#!/home/xmachines-raspi/Python-3.6.13/python

import time
import rospy
from std_msgs.msg import String
import serial
import threading
from nmea_msgs.msg import Sentence
from sensor_msgs.msg import NavSatFix

from pyubx2 import UBXReader

delay_ms = 200
latFromNmea = 0.0
lonFromNmea = 0.0

def convert_deg_to_decimal(degrees):
    decimal_value = 0.0
    try:
        # Split the degrees value into degrees and minutes parts
        degrees_parts = degrees.split(".")

        # Extract the degrees and minutes values
        degrees_value = float(degrees_parts[0][:-2])
        minutes_value = float(degrees_parts[0][-2:] + "." + degrees_parts[1])

        # Calculate the decimal value
        decimal_value = degrees_value + (minutes_value / 60)

    except ValueError:
        pass
    return decimal_value
        

def get_lat_lon(data):
    global latFromNmea, lonFromNmea
    latFromNmea = data.latitude
    lonFromNmea = data.longitude


def get_gps_data():
    lat_ = 0.0
    lon_ = 0.0
    fix_msg = NavSatFix()
    ubr = UBXReader(rover)
    (raw_data, parsed_data) = ubr.read()
    #print(raw_data, len(raw_data), raw_data[1])
    if len(raw_data) == 56 and raw_data[1] == 71:
        text = raw_data.decode("unicode_escape")
        check1 = text.split(',')
        if check1[0] == "$GNGLL":
            lat_ = parsed_data.lat
            lon_ = parsed_data.lon
            fix_msg.header.stamp = rospy.Time.now()
            fix_msg.header.frame_id = "gps"
            fix_msg.latitude = lat_
            fix_msg.longitude = lon_
            fix_pub.publish(fix_msg)
    if len(raw_data) == 44 and raw_data[1] == 98:
        msg = UBXReader.parse(raw_data)
        #print(msg, msg.lon, msg.lat)
        lat_ = msg.lat
        lon_ = msg.lon
        fix_msg.header.stamp = rospy.Time.now()
        fix_msg.header.frame_id = "gps"
        fix_msg.latitude = lat_
        fix_msg.longitude = lon_
        fix_pub.publish(fix_msg)
        
    return lat_, lon_


def send_gps_data():
    global previous_time, exit_flag
    while not exit_flag:
        current_time = rospy.Time.now()
        lat, lon = get_gps_data()
        if lat != 0.0:
            data = '$GPS ' + str(lat) + " " + str(lon) + "\n\r"
            gps_port.write(data.encode())
            #rospy.loginfo(data)
            previous_time = current_time
        

def read_uart_data():
    global exit_flag, lat_, lon_
    rospy.on_shutdown(shutdown_handler)
    try:
        while not exit_flag:
            if gps_port.in_waiting > 0:
                # Read incoming data from UART port
                uart_data = gps_port.readline()
                try:
                    decoded_data = uart_data.decode('utf-8')
                except UnicodeDecodeError:
                    try:
                        decoded_data = uart_data.decode('latin-1')
                    except UnicodeDecodeError:
                        decoded_data = uart_data.decode('ascii', 'ignore')
                if len(decoded_data) > 0:
                    rospy.loginfo("Received data: %s", decoded_data)
                    key_word = decoded_data.split(" ")
                    if key_word[0] == "record":
                        file = open("/home/xmachines-raspi/gps_points.txt", "a")
                        file.write(str(lat_) + " " + str(lon_) + "\n")
                        file.close()
                    if key_word[0] == "clear":
                        file = open("/home/xmachines-raspi/gps_points.txt", "w")
                        file.close()
    except IOError:
        rospy.logerr("IO/Error, passing")

exit_flag = False
def shutdown_handler():
    global exit_flag
    rospy.loginfo("Shutting down GPS node...")
    exit_flag = True
    

gps_port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=0.01)
rover = serial.Serial('/dev/ttyACM0', 9600, timeout=None, parity='N', stopbits=1, bytesize=8, xonxoff=0, rtscts=0)
gps_thread = threading.Thread(target=send_gps_data)
uart_thread = threading.Thread(target=read_uart_data)
fix_pub = rospy.Publisher("/fix", NavSatFix, queue_size=10)
#file = open("/home/xmachines-raspi/gps_points.txt", "w")
#file.close()


if __name__ == '__main__':
    rospy.init_node('gps_node')
    try:
        gps_thread.start()
        uart_thread.start()
        while not exit_flag:
            time.sleep(0.00001)
    except (rospy.ROSInterruptException, KeyboardInterrupt):
        shutdown_handler()
        gps_thread.join()
        uart_thread.join()
        gps_port.close()
        rover.close()
        pass

