#!/usr/bin/env python

import rospy 
from sensor_msgs.msg import Imu
from std_msgs.msg import Bool, Float32
from nav_msgs.msg import Odometry
from xmachines_msgs.msg import debug
from geometry_msgs.msg import Twist
from sensor_msgs.msg import NavSatFix
from geographiclib.geodesic import Geodesic


magnetometer_yaw = 0.0
previous_magnetometer_value = 0.0
mag_count = 0
yaw_threshold = 30.0

t265_odom_x = 0.0
previous_t265_odom_x = 0.0
t265_count = 0
t265_odom_y = 0.0
previous_t265_odom_y = 0.0

difference_in_x_position = 0
difference_in_y_position = 0
x_position_drift_threshold = 0.4
y_position_drift_threshold = 0.4


cmd_vel_x = 0.0
cmd_vel_z = 0.0
cmd_vel_threshold = 0.25
t265_linear_vel_x = 0.0
t265_angular_vel_z = 0.0


ultrasonic_distance = 0.0
ultrasonic_distance_threshold = 80.0


gps_x = 0.0
gps_y = 0.0
gps_count = 0
previous_gps_x = 0.0
previous_gps_y = 0.0
gps_position_threshold = 0.4


debug_msg = debug()

def get_mag_angle(data):
	global magnetometer_yaw, previous_magnetometer_value, mag_count
	magnetometer_yaw = data.orientation.w
	if mag_count == 0:
		previous_magnetometer_value = magnetometer_yaw
		mag_count = mag_count + 1


def check_for_magnetometer_anamoly():
	global magnetometer_yaw, previous_magnetometer_value, mag_count, debug_msg
	if mag_count == 1:
		difference = abs(previous_magnetometer_value - magnetometer_yaw)
		if difference > yaw_threshold:
			#print("mag > thresh",difference, previous_magnetometer_value, magnetometer_yaw)
			previous_magnetometer_value = magnetometer_yaw
			debug_msg.magnetometer.msg = "magnetometer angle anamoly, drift" + str(difference) + "degrees"
			return True
		else:
			#print("mag < thresh",difference, previous_magnetometer_value, magnetometer_yaw)
			previous_magnetometer_value = magnetometer_yaw
			debug_msg.magnetometer.msg = "magnetometer angle fine"
			return False
		
		

def get_t265_odometry(data):
	global t265_odom_x, t265_odom_y, previous_t265_odom_x, previous_t265_odom_y, t265_count
	t265_odom_x = data.pose.pose.position.x
	t265_odom_y = data.pose.pose.position.y
	if t265_count == 0:
		previous_t265_odom_x = t265_odom_x
		previous_t265_odom_y = t265_odom_y
		t265_count = t265_count + 1



def check_for_x_y_anamoly():
	global t265_odom_x, t265_odom_y, previous_t265_odom_x, previous_t265_odom_y, debug_msg, difference_in_x_position, difference_in_y_position
	if t265_count == 1:
		difference_in_x_position == abs(previous_t265_odom_x - t265_odom_x)
		difference_in_y_position == abs(previous_t265_odom_y - t265_odom_y)
		if difference_in_x_position > x_position_drift_threshold or difference_in_y_position > y_position_drift_threshold:
			#print("t265_x_y > thresh", difference_in_x_position, previous_t265_odom_x, t265_odom_x)
		        #print("t265_x_y > thresh", difference_in_y_position, previous_t265_odom_y, t265_odom_y)
			previous_t265_odom_x = t265_odom_x
			previous_t265_odom_y = t265_odom_y
			debug_msg.t265.msg = "t265 positions are abnormal, "+ str(difference_in_x_position) + "m in x " + str(difference_in_y_position) + "m in y"
			return True
		else:
			#print("t265_x_y < thresh", difference_in_x_position, previous_t265_odom_x, t265_odom_x)
			#print("t265_x_y < thresh", difference_in_y_position, previous_t265_odom_y, t265_odom_y)
			previous_t265_odom_x = t265_odom_x
			previous_t265_odom_y = t265_odom_y
			debug_msg.t265.msg = "t265 is fine"
			return False


def get_cmd_vel(data):
	global cmd_vel_x, cmd_vel_z
	cmd_vel_x = data.linear.x
	cmd_vel_z = data.angular.z


def check_for_position_update_anamoly():
	global cmd_vel_x, cmd_vel_z, t265_linear_vel_x, t265_angular_vel_z
	if (cmd_vel_x - cmd_vel_threshold) > t265_linear_vel_x > (cmd_vel_x + cmd_vel_threshold):
		#print("t265_update" ,t265_linear_vel_x,(cmd_vel_x - cmd_vel_threshold), (cmd_vel_x + cmd_vel_threshold))
		debug_msg.t265.msg = "t265 positions is not updating with robots velocity, "
		return True
	else:
		debug_msg.t265.msg = "t265 is fine"
		return False


def check_for_t265_anamoly():
	is_x_y_anamoly_present = check_for_x_y_anamoly()
	is_position_anamoly_present = check_for_position_update_anamoly()
	if is_x_y_anamoly_present or is_position_anamoly_present:
		return True
	else:
		return False 


def get_ultrasonic_distance(data):
	global ultrasonic_distance
	ultrasonic_distance = data.data


def check_ultrasonic_for_obstacle():
	global ultrasonic_distance
	if ultrasonic_distance < ultrasonic_distance_threshold:
		debug_msg.ultrasonic.msg = "ultrasonic detected obstacle in front " + str(ultrasonic_distance) + "cm"
		return True
	else:
		debug_msg.ultrasonic.msg = "no obstacle detected"
		return False


def get_gps_data(data):
	global gps_x, gps_y, previous_gps_x, previous_gps_y, gps_count
	gps_x = data.latitude
	gps_y = data.longitude
	if gps_count == 0:
		previous_gps_x = gps_x
		previous_gps_y = gps_y
        gps_count = gps_count + 1



def check_for_gps_anamoly():
	global gps_x, gps_y, previous_gps_x, previous_gps_y, gps_count, debug_msg, gps_count
	geod = Geodesic.WGS84
	if gps_count == 1:
		data = geod.Inverse(gps_x, gps_y, previous_gps_x, previous_gps_y)
		difference_in_gps = data['s12']
		if difference_in_gps > gps_position_threshold:
			previous_gps_x = gps_x
			previous_gps_y = gps_y
			debug_msg.gps.msg = "gps position anamoly detected, the drift was" + str(difference_in_gps_x) + "m in x "+str(difference_in_gps_y) + "m in y"
			return True
		else:
			previous_gps_x = gps_x
			previous_gps_y = gps_y
			debug_msg.gps.msg = "gps position is fine, accuracy " + str(difference_in_gps_x) + "m in x "+str(difference_in_gps_y) + "m in y"
			return False



if __name__ == '__main__':
	rospy.init_node("watchdog", anonymous=True)
	rospy.Subscriber("/xmachines/imu", Imu, get_mag_angle)
	#rospy.Subscriber("/cam_1/odom/sample", Odometry, get_t265_odometry)
	rospy.Subscriber("/cmd_vel", Twist, get_cmd_vel)
	rospy.Subscriber("/xmachines/ultrasonic_distance", Float32, get_ultrasonic_distance)
	rospy.Subscriber("/fix", NavSatFix, get_gps_data)
	anamoly_publisher = rospy.Publisher("/anamoly_pause", Bool, queue_size=10)
	debug_msg_publisher = rospy.Publisher("/debug_msgs", debug, queue_size=10)
	anamoly_value = Bool()
	rate = rospy.Rate(5)
	while not rospy.is_shutdown():
		is_difference = check_for_magnetometer_anamoly()
		#t265_anamoly = check_for_t265_anamoly()
		is_obstacle_present = check_ultrasonic_for_obstacle()
		gps_anamoly = check_for_gps_anamoly()
		if is_difference or is_obstacle_present or gps_anamoly:
			anamoly_value.data = True
			anamoly_publisher.publish(anamoly_value)
			debug_msg_publisher.publish(debug_msg)
			print("anamoly detected")
		else:
			anamoly_value.data = False
			anamoly_publisher.publish(anamoly_value)
			debug_msg_publisher.publish(debug_msg)
		rate.sleep()