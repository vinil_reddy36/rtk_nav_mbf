#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32
import RPi.GPIO as GPIO
import time


if __name__=='__main__':
	rospy.init_node("ultrasonic_node")
	ultrasonic_publisher = rospy.Publisher("/xmachines/ultrasonic_distance", Float32, queue_size=10)
	ultrasonic_data = Float32()
	GPIO.setmode(GPIO.BCM) 
	GPIO.setwarnings(False)

	TRIG_PIN=11
	ECHO_PIN=13 

	GPIO.setup(TRIG_PIN,GPIO.OUT)
	GPIO.setup(ECHO_PIN, GPIO.IN)
	GPIO.output(TRIG_PIN, GPIO.LOW) 

	time.sleep(2)
	while not rospy.is_shutdown():
		GPIO.output(TRIG_PIN, GPIO.HIGH)
		time.sleep(0.00001)
		GPIO.output(TRIG_PIN, GPIO.LOW)

		while GPIO.input(ECHO_PIN) == 0:
			pulse_start = time.time()

		while GPIO.input(ECHO_PIN) == 1:
			pulse_end = time.time()

		pulse_duration = pulse_end - pulse_start
		distance = pulse_duration * 17150
		distance = round(distance, 2)
		ultrasonic_data.data = distance
		ultrasonic_publisher.publish(ultrasonic_data)
		#print("distance " + str(distance))
		time.sleep(0.1)