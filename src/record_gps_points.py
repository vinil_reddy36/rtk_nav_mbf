#!/usr/bin/env python
import time
import rospy
from sensor_msgs.msg import NavSatFix
from geometry_msgs.msg import Twist


latitude = 0.0
longitude = 0.0
vel_x = 0.0
ang_z = 0.0


def get_current_lat_lon(data):
    global latitude, longitude
    latitude = data.latitude
    longitude = data.longitude


def get_vel(data):
    global vel_x, ang_z
    vel_x = data.linear.x
    ang_z = data.angular.z


if __name__ == '__main__':
    rospy.init_node("gps_data_recorder")
    rospy.Subscriber("/navsat/fix", NavSatFix, get_current_lat_lon)
    rospy.Subscriber("/cmd_vel", Twist, get_vel)
    f = open("gps_points.txt", "a")
    try:
        rospy.loginfo("recording gps points")
        while not rospy.is_shutdown():
            if ang_z > 0.1 or vel_x > 0.1 or vel_x < -0.1 or ang_z < -0.1 and (latitude > 0.0 or latitude < 0.0):
                print(latitude, longitude)
                f.write(str(latitude) + " " + str(longitude) + "\n")
            time.sleep(0.01)
    except KeyboardInterrupt:
        rospy.loginfo("Done recording, saving the file")
        f.close()



