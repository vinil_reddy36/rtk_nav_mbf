#!/usr/bin/env python
# This generates global paths and sends it to move_base_flex.
# code tested on simulation (husky_waypoint_navigation)
# result : working


import math
import time
import rospy
import actionlib
from geodesy import utm
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, PointStamped
from mbf_msgs.msg import ExePathGoal
import mbf_msgs.msg as mbf_msgs
import tf.listener
from tf import transformations
from sensor_msgs.msg import NavSatFix
global current_lat, current_lon
from geographiclib.geodesic import Geodesic

current_lat = 0.0
current_lon = 0.0
number_of_rows = 2
row_spacing = 1.0

B = (49.9000396, 8.9000000)
C = (49.9000407, 8.8999313)


def generate_path(poses, path_, angle):
    q = transformations.quaternion_from_euler(0, 0, angle)
    for x, y, z in poses:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.position.z = z
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    return path_


def get_gps_points_left(pointAlat, pointAlon, pointBlat, pointBlon, pointClat, pointClon, spacing, rows, row_space):
    geod = Geodesic.WGS84
    forward = geod.Inverse(pointAlat, pointAlon, pointBlat, pointBlon)
    backward = geod.Inverse(pointBlat, pointBlon, pointAlat, pointAlon)
    turn =  geod.Inverse(pointBlat, pointBlon, pointClat, pointClon)
    turn_angle = turn['azi1']
    backward_angle = backward['azi1']
    forward_angle = forward['azi1']
    print(forward_angle, backward_angle)
    distance = forward['s12']
    number_of_points = distance / spacing
    print(number_of_points)
    gps_points = []
    arc_length = 2.0
    for count in range(int(number_of_points+1)):
        point = geod.Direct(pointAlat, pointAlon, forward_angle, (spacing * (count + 1)))
        gps_points.append([point['lat2'], point['lon2'], 0])
        nextlat, nextlon = point['lat2'], point['lon2']
    mid_point = geod.Direct(nextlat, nextlon, turn_angle, arc_length)
    for x in ((turn_angle + 180.0 - 45.0), (turn_angle + 180.0 - 90.0)):
        point = geod.Direct(mid_point['lat2'], mid_point['lon2'], x, arc_length)
        gps_points.append([point['lat2'], point['lon2'], 0])
        nextlat, nextlon = point['lat2'], point['lon2']
        if x == (turn_angle + 180.0 - 45.0):
            intersec_lat, intersec_lon = point['lat2'], point['lon2']

    next_mid_point = geod.Direct(nextlat, nextlon, forward_angle, arc_length)
    for x in ((turn_angle - 180.0 + 45.0), (turn_angle - 180.0 + 0.0)):
        point = geod.Direct(next_mid_point['lat2'], next_mid_point['lon2'], x, arc_length)
        gps_points.append([point['lat2'], point['lon2'], 1])
    gps_points.append([intersec_lat, intersec_lon, 0])
    point = geod.Direct(pointBlat, pointBlon, turn_angle, row_space)
    gps_points.append([point['lat2'], point['lon2'], 0])
    for count in range(int(number_of_points+1)):
        point1 = geod.Direct(point['lat2'], point['lon2'], backward_angle, (spacing*(count+1)))
        gps_points.append([point1['lat2'], point1['lon2'], 0])
    print gps_points
    return gps_points


def convert_gps_to_map_points(gps_points):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon, dir in gps_points:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.point.z = dir
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y, point.point.z])
        #print(point.point.x, point.point.y, point.point.z)
    return map_point


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
    rospy.init_node("single_point_nav", anonymous=True)
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    pub_vis = rospy.Publisher("/global_plan", Path, queue_size=10)
    rospy.Subscriber("/navsat/fix", NavSatFix, get_current_lat_lon)
    path_ = Path()
    done = False
    while not rospy.is_shutdown() and done == False:
        print(current_lat, current_lon)
        if current_lat != 0.0:
            latlons = get_gps_points_left(current_lat, current_lon, B[0], B[1], C[0], C[1], 0.5, number_of_rows, row_spacing)
            map_points = convert_gps_to_map_points(latlons)
            yaw = 180.0
            path__ = generate_path(map_points, path_, yaw)
            #for x in path__.poses:
            #    print(x.pose.position.x, x.pose.position.z)
            for x in range(50):
                pub_vis.publish(path__)
                time.sleep(0.1)
            pub.publish(path__)
            done = True
        time.sleep(0.01)