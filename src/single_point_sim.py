#!/usr/bin/env python

# This generates global paths and sends it to move_base_flex.
# code tested on simulation (husky_waypoint_navigation)
# result : working


import math
import time
import rospy
import actionlib
from geodesy import utm
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped, PointStamped
from mbf_msgs.msg import ExePathGoal
import mbf_msgs.msg as mbf_msgs
import tf.listener
from tf import transformations
from sensor_msgs.msg import NavSatFix
global current_lat, current_lon
from geographiclib.geodesic import Geodesic

current_lat = 0.0
current_lon = 0.0

#B = (49.9000344, 8.8999607)
#B = (49.8999996, 8.89998640) #short left
B =  (49.9000419, 8.9000001) # straight
#B = (49.8999992, 8.8999321) # long left


def generate_path(poses, path_, angle):
    q = transformations.quaternion_from_euler(0, 0, angle)
    for x, y in poses:
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = x
        pose.pose.position.y = y
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    return path_


def get_gps_points(pointAlat, pointAlon, pointBlat, pointBlon, spacing):
    geod = Geodesic.WGS84
    inv = geod.Inverse(pointAlat, pointAlon, pointBlat, pointBlon)
    angle = inv['azi1']
    print(angle)
    distance = inv['s12']
    number_of_points = distance / spacing
    gps_points = []
    for i in range(int(number_of_points)):
        point = geod.Direct(pointAlat, pointAlon, angle, (spacing * (i + 1)))
        gps_points.append([point['lat2'], point['lon2']])
    mid_point = geod.Direct(pointBlat, pointBlon, -90.0, 1.0)
    for x in (45.0, 0.0, -45.0, -90.0):
        point = geod.Direct(mid_point['lat2'], mid_point['lon2'], x, 1.0)
        gps_points.append([point['lat2'], point['lon2']])
    return gps_points


def convert_gps_to_map_points(gps_points):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon in gps_points:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y])
    return map_point


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
    rospy.init_node("single_point_nav", anonymous=True)
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    rospy.Subscriber("/navsat/fix", NavSatFix, get_current_lat_lon)
    #mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    #mbf_ep_ac.wait_for_server(rospy.Duration(10))
    #rospy.loginfo("Connected to Move Base Flex ExePath server!")
    #target_path_ = ExePathGoal()
    path_ = Path()
    done = False
    while not rospy.is_shutdown() and done == False:
        print(current_lat, current_lon)
        if current_lat != 0.0:
            latlons = get_gps_points(current_lat, current_lon, B[0], B[1], 0.5)
            map_points = convert_gps_to_map_points(latlons)
            yaw = 180.0
            path__ = generate_path(map_points, path_, yaw)
            for x in path__.poses:
                print(x.pose.position.x)
                print(x.pose.position.y)
            for x in range(50):
                pub.publish(path__)
                time.sleep(0.1)
            #target_path_.path = path__
            #mbf_ep_ac.send_goal(target_path_)
            #mbf_ep_ac.wait_for_result()
            done = True
        time.sleep(0.01)