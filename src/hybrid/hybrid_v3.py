#!/usr/bin/env python
# in this approach a time based planner and our own planner is implemented.
# lino_pid is used instead of regular pid
# stable tested on field

import time
import rospy
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Twist, PoseStamped
from tf import transformations
import math

Kp, Ki, Kd = 0.08, 0.001, 0.1 # stable as previous version, tested 5  rows, less curves initial
#Kp, Ki, Kd = 0.08, 0.001, 0.01 almost stable with initial curves, and then straight tested for 3 rows.
position_x = 0.0
position_y = 0.0
dist_precision_ = 0.5
path_ = Path()
yaw_precision = math.radians(2.0)
yaw_ = 0.0
integral = 0.0
derivative = 0.0
prev_error = 0.0


def get_odometry(data):
    global position_x, position_y, yaw_
    position_x = data.pose.pose.position.x
    position_y = data.pose.pose.position.y
    quaternion = (data.pose.pose.orientation.x,
                  data.pose.pose.orientation.y,
                  data.pose.pose.orientation.z,
                  data.pose.pose.orientation.w)
    yaw_ = transformations.euler_from_quaternion(quaternion)[2]


def get_path(data):
    global path_
    path_ = data


def calculate_error_yaw(required, present):
    # if 0.0 >= required >= -90.0 and 0.0 < present <= 90.0 \
    #         or (0.0 <= required <= 180.0 and 0.0 <= present <= 180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 >= present >= -180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 <= present <= 180.0)\
    #         or (0.0 <= required <= 180.0 and 0.0 >= present >= -180.0):
    if required < -90.0 and present > 90.0:
        return 360.0 + (required - present)
    elif required >= 90.0 and present <= -90.0:
        return (required - present) - 360.0
    else:
        return required - present


def constrain(val, min_val, max_val):
    return min(max_val, max(min_val, val))


def pid_compute(setpoint, measuredValue, min_val, max_val):
    global integral, prev_error
    error = setpoint - measuredValue
    integral = integral + error
    derivative_ = error - prev_error
    pid = (Kp * error) + (Ki * integral) + (Kd * derivative_)
    prev_error = error
    return constrain(pid, min_val, max_val)


def own_planner(point, vel):
    global yaw_, yaw_precision
    twist_msg = Twist()
    err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                        pow(point.pose.position.x - position_x, 2))
    rate = rospy.Rate(20)
    while err_pos > dist_precision_:
        desired_yaw = math.degrees(math.atan2(point.pose.position.y - position_y,
                                              point.pose.position.x - position_x))
        err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                            pow(point.pose.position.x - position_x, 2))

        err_yaw = calculate_error_yaw(desired_yaw, math.degrees(yaw_))
        twist_msg.linear.x = 0.3
        output = pid_compute(0.0, err_yaw, -0.6, 0.6)
        twist_msg.angular.z = -output
        print(desired_yaw, math.degrees(yaw_), output)
        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)


def generate_path(current_x, current_y, goal_x, goal_y):
    path__ = Path()
    distance = math.sqrt(((goal_y - current_y) ** 2) + ((goal_x - current_x) ** 2))
    diff_x = goal_x - current_x
    diff_y = goal_y - current_y
    point_num = distance / 0.2
    intervel_x = diff_x / point_num
    intervel_y = diff_y / point_num
    desired_yaw = math.degrees(math.atan2(goal_y - current_y,
                                          goal_x - current_x))
    q = transformations.quaternion_from_euler(0, 0, desired_yaw)
    for i in range(int(point_num)):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = current_x + intervel_x * i
        pose.pose.position.y = current_y + intervel_y * i
        pose.pose.orientation.x = q[0]
        pose.pose.orientation.y = q[1]
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path__.header.frame_id = "map"
        path__.header.stamp = rospy.Time.now()
        path__.poses.append(pose)
    return path__


def timer_based_planner(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(150):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(110):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


def timer_based_planner_r(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(100):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(100):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


if __name__ == '__main__':
    rospy.init_node("hybrid_local_planner_v3", anonymous=True)
    rospy.Subscriber("/odometry/filtered_map", Odometry, get_odometry)
    rospy.Subscriber("/global_plan_proxy", Path, get_path)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    # mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    # mbf_ep_ac.wait_for_server(rospy.Duration(10))
    # rospy.loginfo("Connected to Move Base Flex ExePath server!")
    while not rospy.is_shutdown():
        if len(path_.poses) > 1:
            for x in path_.poses[::1]:
                print(x.pose.position.x, x.pose.position.y, x.pose.position.z)
                if x.pose.position.z == 0.01:
                    rospy.loginfo("using own planner")
                    own_planner(x, cmd_vel)
                if x.pose.position.z == 0.02:
                    rospy.loginfo("using move_base_flex")
                    # move_base_flex_planner(mbf_ep_ac, x.pose.position.x, x.pose.position.y)
                if x.pose.position.z == -0.03:
                    rospy.loginfo("using time based planner")
                    timer_based_planner(cmd_vel, -1)
                if x.pose.position.z == 0.03:
                    rospy.loginfo("using time based planner")
                    timer_based_planner(cmd_vel, 1)
                if x.pose.position.z == -0.04:
                    rospy.loginfo("using time based planner -0.04")
                    timer_based_planner_r(cmd_vel, -1)
                if x.pose.position.z == 0.04:
                    rospy.loginfo("using time based planner 0.04")
                    timer_based_planner_r(cmd_vel, 1)
            break
        time.sleep(0.01)
