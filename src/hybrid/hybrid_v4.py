#!/usr/bin/env python
# in this approach a time based planner and our own planner is implemented.
# yaw_precision is taken into consideration i.e,
# if error yaw is greater than yaw_precision, apply PID
# else make output to 0


import time
import rospy
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import Twist, PoseStamped
from mbf_msgs.msg import ExePathGoal
from tf import transformations
import math
from simple_pid import PID


#pid = PID(0.025, 0.001, 0.05, setpoint=1)
pid = PID(0.055, 0.00, 0.01, setpoint=1)
position_x = 0.0
position_y = 0.0
dist_precision_ = 0.4
path_ = Path()
yaw_precision = math.radians(2.0)
yaw_ = 0.0


def get_odometry(data):
    global position_x, position_y, yaw_
    position_x = data.pose.pose.position.x
    position_y = data.pose.pose.position.y
    quaternion = (data.pose.pose.orientation.x,
                  data.pose.pose.orientation.y,
                  data.pose.pose.orientation.z,
                  data.pose.pose.orientation.w)
    yaw_ = transformations.euler_from_quaternion(quaternion)[2]


def get_path(data):
    global path_
    path_ = data


def calculate_error_yaw(required, present):
    # if 0.0 >= required >= -90.0 and 0.0 < present <= 90.0 \
    #         or (0.0 <= required <= 180.0 and 0.0 <= present <= 180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 >= present >= -180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 <= present <= 180.0)\
    #         or (0.0 <= required <= 180.0 and 0.0 >= present >= -180.0):
    if required < -90.0 and present > 90.0:
        return 360.0 + (required - present)
    elif required >= 90.0 and present <= -90.0:
        return (required - present) - 360.0
    else:
        return required - present


def own_planner(point, vel):
    global yaw_, yaw_precision, state_
    twist_msg = Twist()
    err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                        pow(point.pose.position.x - position_x, 2))
    rate = rospy.Rate(20)
    while err_pos > dist_precision_:
        desired_yaw = math.degrees(math.atan2(point.pose.position.y - position_y,
                                              point.pose.position.x - position_x))
        err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                            pow(point.pose.position.x - position_x, 2))

        err_yaw = calculate_error_yaw(desired_yaw, math.degrees(yaw_))
        if math.fabs(err_yaw) > math.degrees(yaw_precision):
            twist_msg.linear.x = 0.3
            pid.setpoint = 0
            pid.output_limits = (-0.6, 0.6)
            output = pid(err_yaw)
        #print(err_pos, err_yaw, output)
        twist_msg.angular.z = -output
        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)


def generate_path(current_x, current_y, goal_x, goal_y):
    path_ = Path()
    distance = math.sqrt(((goal_y - current_y) ** 2) + ((goal_x - current_x) ** 2))
    diff_x = goal_x - current_x
    diff_y = goal_y - current_y
    point_num = distance / 0.2
    intervel_x = diff_x / point_num
    intervel_y = diff_y / point_num
    desired_yaw = math.degrees(math.atan2(goal_y - current_y,
                                          goal_x - current_x))
    q = transformations.quaternion_from_euler(0, 0, desired_yaw)
    for i in range(int(point_num)):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = current_x + intervel_x * i
        pose.pose.position.y = current_y + intervel_y * i
        pose.pose.orientation.x = q[0]
        pose.pose.orientation.y = q[1]
        pose.pose.orientation.z = q[2]
        pose.pose.orientation.w = q[3]
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    return path_


def move_base_flex_planner(mbf_ep_ac_, goal_x, goal_y):
    global position_x, position_y
    target_path_ = ExePathGoal()
    generated_path = generate_path(position_x, position_y, goal_x, goal_y)
    target_path_.path = generated_path
    mbf_ep_ac_.send_goal(target_path_)
    rospy.loginfo("goal sent")
    mbf_ep_ac_.wait_for_result()
    rospy.loginfo("function exited")


def timer_based_planner(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(150):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(110):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = 0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


def timer_based_planner_r(cmd_vel__, mul):
    twist_msgs = Twist()
    if mul > 0:
        for i in range(130):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    if mul < 0:
        for i in range(100):
            twist_msgs.linear.x = 0.3 * mul
            twist_msgs.angular.z = -0.3
            cmd_vel__.publish(twist_msgs)
            time.sleep(0.1)
    for i in range(3):
        twist_msgs.linear.x = 0
        twist_msgs.angular.z = 0
        cmd_vel__.publish(twist_msgs)
        time.sleep(0.5)


if __name__ == '__main__':
    rospy.init_node("hybrid_local_planner", anonymous=True)
    rospy.Subscriber("/odometry/filtered_map", Odometry, get_odometry)
    rospy.Subscriber("/global_plan_proxy", Path, get_path)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    #mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    #mbf_ep_ac.wait_for_server(rospy.Duration(10))
    #rospy.loginfo("Connected to Move Base Flex ExePath server!")
    while not rospy.is_shutdown():
        if len(path_.poses) > 1:
            for x in path_.poses[::1]:
                if x.pose.position.z == 0.01:
                    rospy.loginfo("using own planner")
                    own_planner(x, cmd_vel)
                if x.pose.position.z == 0.02:
                    rospy.loginfo("using move_base_flex")
                    #move_base_flex_planner(mbf_ep_ac, x.pose.position.x, x.pose.position.y)
                if x.pose.position.z == -0.03:
                    rospy.loginfo("using time based planner")
                    timer_based_planner(cmd_vel, -1)
                if x.pose.position.z == 0.03:
                    rospy.loginfo("using time based planner")
                    timer_based_planner(cmd_vel, 1)
                if x.pose.position.z == -0.04:
                    rospy.loginfo("using time based planner -0.04")
                    timer_based_planner_r(cmd_vel, -1)
                if x.pose.position.z == 0.04:
                    rospy.loginfo("using time based planner 0.04")
                    timer_based_planner_r(cmd_vel, 1)
            break
        time.sleep(0.01)




