import serial
import time
import numpy as np
float_values = [487.50,  138.36, -115.49, 0.60209,  0.01347, -0.00461, 0.01347,  0.60849,  0.00139, -0.00461, 0.00139,  0.60327, 39.17,    8.76,  -18.16, 1.64410,  0.00741, -0.00338, 0.00741,  1.65496,  0.09194, -0.00338,  0.09194,  1.67879, -244.2, 91.7, -72.4]

def check_(data):
    global float_values
    float_array = []
    array = data.split(" ")
    array = array[:-1]
    for s in array:
        f = float(s)
        float_array.append(f)
    print(float_array)
    if float_values == float_array:
        return True



# Define the serial port name and baud rate
ser = serial.Serial('/dev/ttyUSB0', 57600, timeout=2)
# Define the 27 float values
time.sleep(2)
# Convert the float values to a long string separated by spaces
# Send the string to the serial port
data = "ok"
ser.write(data.encode())
time.sleep(0.01)
#b = ser.readline()
#print(b)
#check = check_(b)
float_str = " ".join(str(f) for f in float_values)
float_str = float_str + "\n"
print(float_str)
check = False
calib = False
while not calib:
    #ser.write("ok\n".encode())
    #ser.write(float_str.encode())
    b = ser.readline()
    print(b)
    if b == "over\r\n":
        calib = True

while not check and calib:
        ser.write(float_str.encode())
        b = ser.readline()
        try:
            check = check_(b)
            print("checking")
        except ValueError:
            continue
        if check == True:
            test = "over"
            print("sended over")
            ser.write(test.encode())       

try:
    while True:
        b = ser.readline()
        print(b)
except KeyboardInterrupt:
    ser.close()
time.sleep(0.01)