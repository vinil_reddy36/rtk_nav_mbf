
import serial
import time
import pandas as pd 



acc_list = []
mag_list = []

def update_csv(data, ser):
    global acc_list, mag_list
    array = data.split(" ")
    print(array)
    if array[0] == 'Done.':
        data_ = "okk"
        ser.write(data_.encode())
    if len(array) == 6:
        acc_list.append([array[0], array[1], array[2]])
        mag_list.append([array[3], array[4], array[5].rstrip()])
        print(int(array[5]))

if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyUSB0', 57600, timeout=1)
    time.sleep(2)
    data = "ok"
    ser.write(data.encode())
    try:
        while True: 
            b = ser.readline()
            print(b)
            update_csv(b, ser)
    except KeyboardInterrupt:
        acce_ = pd.DataFrame(acc_list)
        acce_.to_csv("acc.csv", header=False, index=False)
        mag_ = pd.DataFrame(mag_list)
        mag_.to_csv("mag.csv", header=False, index=False)
        ser.close()
