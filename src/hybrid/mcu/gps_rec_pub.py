#!/usr/bin/python

import rospy
from std_msgs.msg import Float32MultiArray



pub_data = [[0.0, 0.0]] * 4
if __name__ == '__main__':
    rospy.init_node("gps_recorded_publisher")
    pub = rospy.Publisher("/recorded_gps_points", Float32MultiArray, queue_size=10)
    msg = Float32MultiArray()
    rate = rospy.Rate(10)
    while not rospy.is_shutdown():
        f = open("/home/vinil/gps_points.txt", "r")
        txt = f.read()
        txt = txt.split("\n")
        if len(txt) >= 4:
            lat_lons = []
            for i in txt:
                if len(i) > 0:
                    a = i.split(" ")
                    #print(a[0], a[1])
                    lat_lons.append(float(a[0]))
                    lat_lons.append(float(a[1]))
            #print(lat_lons)
            msg.data = lat_lons
            pub.publish(msg)
        rate.sleep()



