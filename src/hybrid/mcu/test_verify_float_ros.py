#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32MultiArray

rospy.init_node('float_array_publisher')

# Define the Float32MultiArray message
float_array_msg = Float32MultiArray()

# Define the array of float values
float_array = [487.50,  138.36, -115.49, 0.60209,  0.01347, -0.00461, 0.01347,  0.60849,  0.00139, -0.00461, 0.00139,  0.60327, 39.17,    8.76,  -18.16, 1.64410,  0.00741, -0.00338, 0.00741,  1.65496,  0.09194, -0.00338,  0.09194,  1.67879, -244.2, 91.7, -72.4]
# Populate the Float32MultiArray message with the float array values
float_array_msg.data = float_array

# Set the data length of the Float32MultiArray message

# Define the publisher
pub = rospy.Publisher('/xmachines/correction_data', Float32MultiArray, queue_size=1)

while not rospy.is_shutdown():
# Publish the Float32MultiArray message
    pub.publish(float_array_msg)
    rospy.sleep(1)
# Wait for the message to be published before exiting
