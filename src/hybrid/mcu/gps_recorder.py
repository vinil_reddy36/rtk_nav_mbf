#!/usr/bin/env python

import time
import rospy
from sensor_msgs.msg import NavSatFix
import RPi.GPIO as GPIO # Import Raspberry Pi GPIO library

GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

current_lat = 0.0
current_lon = 0.0
vel_x = 0.0
ang_z = 0.0


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


if __name__ == '__main__':
    rospy.Subscriber("/fix", NavSatFix, get_current_lat_lon)
    while not rospy.is_shutdown():
        if GPIO.input(10) == GPIO.HIGH:
            f = open("gps_points.txt", "a")
            f.write(str(current_lat) + " " + str(current_lon) + "\n")
            f.close()
        if GPIO.input(12) == GPIO.HIGH:
            file = open("gps_points.txt", "w")
            file.close()

    time.sleep(0.01)