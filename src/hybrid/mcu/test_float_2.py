import serial
import time
import pandas as pd 
import subprocess


float_values = [None] * 27
acc_list = []
mag_list = []
gyro_off = []


def check_(data):
    global float_values
    count = 0
    float_array = []
    array = data.split(" ")
    array = array[:-1]
    for s in array:
        f = float(s)
        float_array.append(f)
    #print(float_values)
    #print(float_array)
    if len(float_array) == 27:
        for i in range(len(float_array)):
            print(round(float_array[i],2), round(float_values[i], 2), round(float_array[i],2) == round(float_values[i], 2))
            if round(float_array[i],2) == round(float_values[i], 2):
                count = count + 1
    if count == 27:
        return True
    # if float_values == float_array:
    #     return True


def update_data(data):
    global acc_list, mag_list, float_values
    array = data.split(" ")
    #print(array)
    if array[0] == 'Gyrodone':
        print("done gyro calib")
        print(array)
        print(type(array[1]))
        float_values[24] = float(array[1])
        float_values[25] = float(array[2])
        float_values[26] = float(array[3].rstrip())
        #gyro_off.append([array[1], array[2], array[3]])
    if len(array) == 6:
        acc_list.append([array[0], array[1], array[2]])
        mag_list.append([array[3], array[4], array[5].rstrip()])
        #print(int(array[5]))

def update_mag_data(data):
    global float_values
    array = data.split(",")
    #print(array)
    float_values[12] = float(array[0])
    float_values[13] = float(array[1])
    float_values[14] = float(array[2])
    float_values[15] = float(array[3])
    float_values[16] = float(array[4])
    float_values[17] = float(array[5])
    float_values[18] = float(array[6])
    float_values[19] = float(array[7])
    float_values[20] = float(array[8])
    float_values[21] = float(array[9])
    float_values[22] = float(array[10])
    float_values[23] = float(array[11])



def update_accel_data(data):
    global float_values
    array = data.split(",")
    #print(array)
    float_values[0] = float(array[0])
    float_values[1] = float(array[1])
    float_values[2] = float(array[2])
    float_values[3] = float(array[3])
    float_values[4] = float(array[4])
    float_values[5] = float(array[5])
    float_values[6] = float(array[6])
    float_values[7] = float(array[7])
    float_values[8] = float(array[8])
    float_values[9] = float(array[9])
    float_values[10] = float(array[10])
    float_values[11] = float(array[11])




def get_mag_acc_calib_data():
    c_program = "/home/vinil/linorobot_ws/src/rtk_nav_mbf/src/firmware/MPU-9250-AHRS-master/magneto/magneto_accel" # Replace with the name of your C program
    output = subprocess.check_output(c_program, shell=True)
    update_accel_data(output.decode("utf-8"))
    #print(output.decode())
    c_program = "/home/vinil/linorobot_ws/src/rtk_nav_mbf/src/firmware/MPU-9250-AHRS-master/magneto/magneto_mag" # Replace with the name of your C program
    output = subprocess.check_output(c_program, shell=True)
    update_mag_data(output.decode("utf-8"))
    #print(output.decode())

ser = serial.Serial('/dev/ttyUSB0', 57600, timeout=2)
time.sleep(2)

check = False
calib = False
while not calib:
    b = ser.readline()
    #print(b)
    update_data(b)
    if b == "over\r\n":
        calib = True
        print("creating accel.csv")
        acce_ = pd.DataFrame(acc_list)
        acce_.to_csv("acc.csv", header=False, index=False)
        print("creating mag.csv")
        mag_ = pd.DataFrame(mag_list)
        mag_.to_csv("mag.csv", header=False, index=False)
        print("running madgwick filter on mag.csv and acc.csv")
        get_mag_acc_calib_data()
        print("calibration done toggle the switch")


float_str = " ".join(str(f) for f in float_values)
float_str = float_str + "\n"
print(float_str)
while not check and calib:
        ser.write(float_str.encode())
        b = ser.readline()
        try:
            check = check_(b)
            print("checking")
        except ValueError:
            continue
        if check == True:
            test = "over"
            print("sended over")
            ser.write(test.encode())    
        time.sleep(0.5)   

try:
    while True:
        b = ser.readline()
        print(b)
except KeyboardInterrupt:
    ser.close()
time.sleep(0.01)










