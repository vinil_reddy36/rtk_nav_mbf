#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32MultiArray, Bool
import pandas as pd 
import subprocess
import time

acc_list = []
mag_list = []
gyro_off = []
calib_status = False
recieved_correction_status = False
float_values = [None] * 27



def get_calib_data(data):
    global acc_list, mag_list, float_values
    acc_list.append([data.data[0], data.data[1], data.data[2]])
    mag_list.append([data.data[3], data.data[4], data.data[5]])
    float_values[24] = float(data.data[6])
    float_values[25] = float(data.data[7])
    float_values[26] = float(data.data[8])


def get_calib_status(data):
    global calib_status
    calib_status = data.data


def update_mag_data(data):
    global float_values
    array = data.split(",")
    print(array)
    float_values[12] = float(array[0])
    float_values[13] = float(array[1])
    float_values[14] = float(array[2])
    float_values[15] = float(array[3])
    float_values[16] = float(array[4])
    float_values[17] = float(array[5])
    float_values[18] = float(array[6])
    float_values[19] = float(array[7])
    float_values[20] = float(array[8])
    float_values[21] = float(array[9])
    float_values[22] = float(array[10])
    float_values[23] = float(array[11])



def update_accel_data(data):
    global float_values
    array = data.split(",")
    print(array)
    float_values[0] = float(array[0])
    float_values[1] = float(array[1])
    float_values[2] = float(array[2])
    float_values[3] = float(array[3])
    float_values[4] = float(array[4])
    float_values[5] = float(array[5])
    float_values[6] = float(array[6])
    float_values[7] = float(array[7])
    float_values[8] = float(array[8])
    float_values[9] = float(array[9])
    float_values[10] = float(array[10])
    float_values[11] = float(array[11])



def get_mag_acc_calib_data():
    c_program = "/home/vinil/linorobot_ws/src/rtk_nav_mbf/src/firmware/MPU-9250-AHRS-master/magneto/magneto_accel" # Replace with the name of your C program
    output = subprocess.check_output(c_program, shell=True)
    update_accel_data(output.decode("utf-8"))
    print(output.decode())
    c_program = "/home/vinil/linorobot_ws/src/rtk_nav_mbf/src/firmware/MPU-9250-AHRS-master/magneto/magneto_mag" # Replace with the name of your C program
    output = subprocess.check_output(c_program, shell=True)
    update_mag_data(output.decode("utf-8"))
    print(output.decode())


def send_calib_corrected(pub, msg):
    global float_values
    msg.data = float_values
    pub.publish(msg)


def get_recv_status(data):
    global recieved_correction_status
    recieved_correction_status = data.data

if __name__ == '__main__':
    rospy.init_node("test")
    calib_corrected_pub = rospy.Publisher("/xmachines/correction_data", Float32MultiArray, queue_size=1)
    mode_pub = rospy.Publisher("/xmachines/mode", Bool, queue_size=1)
    mode_msg = Bool()
    rospy.Subscriber("/xmachines/calib", Float32MultiArray, get_calib_data)
    rospy.Subscriber("/xmachines/calib_done", Bool, get_calib_status)
    calib_msg = Float32MultiArray()
    while not rospy.is_shutdown():
        if calib_status:
            print(calib_status, recieved_correction_status)
            print("creating accel.csv")
            acce_ = pd.DataFrame(acc_list)
            acce_.to_csv("acc.csv", header=False, index=False)
            print("creating mag.csv")
            mag_ = pd.DataFrame(mag_list)
            mag_.to_csv("mag.csv", header=False, index=False)
            print("applying madgwick filter")
            get_mag_acc_calib_data()
            print("sending data")
            time.sleep(2)
            send_calib_corrected(calib_corrected_pub, calib_msg)
        time.sleep(1)
print("done")