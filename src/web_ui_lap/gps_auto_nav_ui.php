<!DOCTYPE html>
<html>
	<head>
        <style>
            .lbtn-group{
              position: absolute;
              top: 800px;
              left: 10px;
              margin-bottom: 10px;
              color: white; /* White text */
              padding: 10px 54px; /* Some padding */
              cursor: pointer; /* Pointer/hand icon */
              display: block; /* Float the buttons side by side */
              float: left;
            }

        		.button {
        		position: relative;
          		display: inline-block;
          		margin-bottom: 20px;
        		  padding: 15px 25px;
        		  font-size: 24px;
        		  cursor: pointer;
        		  text-align: center;
        		  text-decoration: none;
        		  outline: none;
        		  color: #fff;
        		  background-color: #777;
        		  border: none;
        		  border-radius: 15px;
        		  box-shadow: 0 15px #999;
        		}
        		.btn-group .button {
                  background-color: #4CAF50; /* Green */
                  border: 1px solid white;
                  color: white;
                  padding: 15px 32px;
                  text-align: center;
                  text-decoration: none;
                  display: inline-block;
                  font-size: 26px;
                  cursor: pointer;
                  float: left;
                  box-shadow: 0 9px #999

                }
            .btn-group .button_pause{
               background-color: #4CAF50; /* Green */
               border: 1px solid white;
               color: white;
               padding: 15px 32px;
               text-align: center;
               text-decoration: none;
               display: inline-block;
               font-size: 26px;
               cursor: pointer;
               float: left;
               box-shadow: 0 9px #999

            }

            .btn-group .button:not(:last-child) {
              border-right: none; /* Prevent double borders */
            }

            .btn-group .button:hover {
              background-color: #3e8e41;
            }
            .btn-group .button:active {
              background-color: #3e8e41;
              box-shadow: 0 5px #666;
              transform: translateY(4px);
            }
                    .button_ {
        		display: inline-block;
        	/* add more crazy CSS3 stuff like rounded corners and gradients... */
    	     }
	</style>
</head>
		<body>
			<script type="text/javascript" src="http://192.168.41.191/eventemitter2.min.js"></script>
			<script type="text/javascript" src="http://192.168.41.191/roslib.min.js"></script>
			<?php
                                $stop_clicked = true;
   				chdir('/home/xmachines/xmachines_ws/src/weed_killer/src/service_scripts');
   				if(isset($_POST['start'])) {
     					$output=shell_exec("echo '1' > pre_emergence.txt");
     					echo $output;
				        $stop_clicked = false;
   				}
   				if(isset($_POST['stop'])) {
     					$output=shell_exec("echo '0' > pre_emergence.txt");
     					echo $output;
					$stop_clicked = true;
   				}
  			?>


  			<form class="button_" action="" method="post">
    				<input type="submit" name="start" value="START" style="height: 200px; width: 200px;">
  			</form>
  			<form class="button_"  action="" method="post">
    				<input type="submit" name="stop" value="STOP" style="height: 200px; width: 200px;">
  			</form>

			<canvas  id="voltage" width="300" height="300" style="position: absolute; border:5px solid #d3d3d3; left: 950px;">
			Your browser does not support the HTML5 canvas tag.</canvas>
			<canvas  id="current" width="300" height="300" style="position: absolute; border:5px solid #d3d3d3; left: 600px;">
			Your browser does not support the HTML5 canvas tag.</canvas>

            <br><br>

            Select Point A<input type="text" id="point_a" value="0.0"><br><br><br>
            Select Point B<input type="text" id="point_b" value="0.0"><br><br><br>
            Row Length<input type="text" id="row_length" value="0.0"><br><br><br>
            Number of Rows<input type="text" id="no_of_rows" value="0.0"><br><br><br>
            First Turn<input type="text" id="first_turn" value="0.0"><br><br><br>
            <button id="send" class="button" onclick="send_ros()" touchstart="send_ros()">send</button>
            <script>
                  var ip = "192.168.41.191";
                  var ros = new ROSLIB.Ros({
                  url : 'ws://'+ip+':9090'
                  });

                  ros.on('connection', function() {
                  console.log('Connected to websocket server.');
                  });

                  ros.on('error', function(error) {
                  console.log('Error connecting to websocket server: ', error);

                  });

                  ros.on('close', function() {
                  console.log('Connection to websocket server closed.');
                  var variable = <?php echo json_encode($stop_clicked); ?>;
                  if (variable){
                  var ding = true;
                  } else {
                   document.location.reload(true);
                  }
                  });

	        function send_ros(){
	        var point_a = parseFloat(document.getElementById("point_a").value);
	        var point_b = parseFloat(document.getElementById("point_b").value);
	        var row_length = parseFloat(document.getElementById("row_length").value);
          var no_of_rows = parseFloat(document.getElementById("no_of_rows").value);
          var first_turn = parseFloat(document.getElementById("first_turn").value);

	        var coordinates = [sprayer_width, rfr, area, 1.0];
			var user_input = new ROSLIB.Topic({
				      ros : ros,
				      name : '/pre_emergence_data',
				      messageType : 'std_msgs/Float32MultiArray'
				    });

				   var state = new ROSLIB.Message({
				      data : coordinates
				    });


                      user_input.publish(state);
                }
				var battery = new ROSLIB.Topic({
					ros:ros,
					name : '/xmachines/battery',
					messageType : 'xmachines_msgs/Battery'
                                       });

				battery.subscribe(function(message){
					voltage = message.battery_voltage;
					current = message.current_draw;
					display_voltage(voltage);
					display_current(current);
					});

				function display_voltage(Voltage){
  				var V = document.getElementById("voltage");
  				var ctx1 = V.getContext("2d")
  				var ctx2 = V.getContext("2d")
  				ctx1.clearRect(0,0,V.width,V.height);
  				var a = convertRange(Voltage, [ 0, 40 ], [ 0.7*Math.PI, 2.3 * Math.PI ] );
  				ctx2.beginPath();
  				if(a < 4.0){
  				ctx2.arc(150,150, 100, 0.7*Math.PI, a);
  				ctx2.strokeStyle = "red";
  				ctx2.lineWidth = 10;
  				ctx2.stroke();
  				ctx1.font = "30px Arial";
  				ctx1.fillText(Math.round(Voltage*10)/10,120,150);
  				} else {
  				ctx1.clearRect(0,0,V.width,V.height);
  				ctx2.arc(150, 150, 100, 0.7*Math.PI, 4.0);
  				ctx2.strokeStyle = "green";
  				ctx2.lineWidth = 10;
  				ctx2.stroke();
  				ctx1.beginPath();
  				ctx1.arc(150, 150, 100, 4.0, a);
  				ctx1.strokeStyle = "green";
  				ctx1.lineWidth = 10;
  				ctx1.stroke();
  				ctx1.font = "30px Arial";
  				ctx1.fillText(Math.round(Voltage*10)/10 + "V",120,150);
  				}
  				}
  				function display_current(Current){
  				if(Current < 0){
  				   Current = Math.abs(Current);
  				}
  				var I = document.getElementById("current");
  				var Ictx1 = I.getContext("2d")
  				var Ictx2 = I.getContext("2d")
  				Ictx1.clearRect(0,0,I.width,I.height);
  				var a = convertRange(Current, [0, 25], [ 0.7*Math.PI, 2.3 * Math.PI ] );

  				Ictx2.beginPath();
  				if(a < 4.0){
  				Ictx2.arc(150, 150, 100, 0.7*Math.PI, a);
  				Ictx2.strokeStyle = "green";
  				Ictx2.lineWidth = 10;
  				Ictx2.stroke();
  				Ictx1.font = "30px Arial";
  				Ictx1.fillText(Math.round(Current*10)/10 + "A",120,150);
  				} else {
  				Ictx1.clearRect(0,0,I.width,I.height);
  				Ictx2.arc(150, 150, 100, 0.7*Math.PI, 4.0);
  				Ictx2.strokeStyle = "red";
  				Ictx2.lineWidth = 10;
  				Ictx2.stroke();
  				Ictx1.beginPath();
  				Ictx1.arc(150, 150, 100, 4.0, a);
  				Ictx1.strokeStyle = "red";
  				Ictx1.lineWidth = 10;
  				Ictx1.stroke();
  				Ictx1.font = "30px Arial";
  				Ictx1.fillText(Math.round(Current*10)/10 + "A",120,150);
  				}
  				}
  				function convertRange( value, r1, r2 ) {
  				    return ( value - r1[ 0 ] ) * ( r2[ 1 ] - r2[ 0 ] ) / ( r1[ 1 ] - r1[ 0 ] ) + r2[ 0 ];
  				}

				</script>

		</body>
</html>
