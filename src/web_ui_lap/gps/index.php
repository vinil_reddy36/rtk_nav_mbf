<!DOCTYPE HTML>
<html>
    <style>
        #map { height: 380px; width:50%}
    </style>
    
<head>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
    integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
     integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM="
     crossorigin=""></script>
<title>OpenLayers Simplest Example</title>
</head>
<body onload="send_to_database()">
    <div id="map"></div>
    17.519116 , 78.273261
</body>
<script type="text/javascript" src="http://192.168.29.244/eventemitter2.min.js"></script>
<script type="text/javascript" src="http://192.168.29.244/roslib.min.js"></script>
<script type="text/javascript" src="http://192.168.29.244/gps/MovingMarker.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<Script>
                    var ip = location.host;
                    var connected = true;
                    var lat = 0.0;
                    var lon = 0.0;
                    var line = "";
                    var map = "";
				    var ros = new ROSLIB.Ros({
				    	url : 'ws://'+ip+':9090'
					});

				       ros.on('connection', function() {
					console.log('Connected to websocket server.');
				       });

				       ros.on('error', function(error) {
					console.log('Error connecting to websocket server: ', error);

				       });

				       ros.on('close', function() {
				    	console.log('Connection to websocket server closed.');

				       });

                       var listener = new ROSLIB.Topic({
					    ros : ros,
					    name : '/navsat/fix',
					    messageType : 'sensor_msgs/NavSatFix'
					  });
                      
					  listener.subscribe(function(message) {
                        lat = message.latitude;
                        lon = message.longitude;
                        display(lat, lon);
                        marker.setLatLng([lat, lon]).update();
                        update_green_path();
					  });

                      var path = new ROSLIB.Topic({
					    ros : ros,
					    name : '/global_plan/gps_ui',
					    messageType : 'nav_msgs/Path'
					  });
					  path.subscribe(function(message) {
                        display_path(message);
                        
					  });


        function update_green_path(){
            var d = "get";
            $.ajax({
                url: 'gps_ui.php',
                type: 'post',
                data: { "update_pos": d },
                success: function (response) {
                        var line_ = response;
                        if (line_ != "0 results"){
                        //console.log(line_);
                        display_green_path(line_.split("/n"))
                    }
                    
        }});
        }
        function send_to_database() {
            var d = "get_list";
            $.ajax({
                url: 'gps_ui.php',
                type: 'post',
                data: { "get_data": d },
                success: function (response) {
                    var line_ = response.split("/n");
                    //console.log(line_);
                    display_path(line_)
                    line = L.polyline([], {
                            color: 'green',
                            weight: 9,
                            opacity: 1,
                            smoothFactor: 1
                            }).addTo(map);
                    
        }});
        }                
        function display(lat, lon){
            if(connected){
            map = L.map('map').setView([lat , lon], 19);
            
            L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 4,
                maxZoom: 22,

                maxNativeZoom: 19,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
            marker = L.marker(map.getCenter()).addTo(map)
            connected = false;
            }
        }
    function display_green_path(points){
        display_points = []
        for (let i = 0; i < points.length - 1; i++){
            var test = points[i].split(" ");
            if(test[2] != -0.03 && test[2] != 0.03 && test[2] != -0.04 && test[2] != 0.04){
                display_points.push([parseFloat(test[0]), parseFloat(test[1])])
            }
        }
        var firstpolyline = new L.Polyline(display_points, {
        color: 'green',
        weight: 3,
        opacity: 0.5,
        smoothFactor: 1
        });
        firstpolyline.addTo(map);
    }
    function display_path(points){
        display_points = []
        for (let i = 0; i < points.length - 1; i++){
            var test = points[i].split(" ");
            if(test[2] != -0.03 && test[2] != 0.03 && test[2] != -0.04 && test[2] != 0.04){
                display_points.push([parseFloat(test[0]), parseFloat(test[1])])
            }
        }
        var firstpolyline = new L.Polyline(display_points, {
        color: 'red',
        weight: 3,
        opacity: 0.5,
        smoothFactor: 1
        });
        firstpolyline.addTo(map);
        
    }


// var i = 0, howManyTimes = 100;

// function f() {
//   console.log("hi");
//   marker.setLatLng([17.519116 + (i * 0.00001), 78.273261]).update();
//   i++;
//   if (i < howManyTimes) {
//     setTimeout(f, 100);
//   }
// }
// f();


// var points = [];

// for (let i = 0 ; i < 100 ; i++){
//     var point = [17.519116 + (i * 0.00001), 78.273261];
//     points.push(point);
// }

// var firstpolyline = new L.Polyline(points, {
//     color: 'red',
//     weight: 3,
//     opacity: 0.5,
//     smoothFactor: 1
// });


// firstpolyline.addTo(map);




</Script>
</html>