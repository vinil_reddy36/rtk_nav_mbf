#!/usr/bin/env python
from geographiclib.geodesic import Geodesic
import random

pointA = (17.5190915, 78.2733940)
#pointB = (17.5191036, 78.2733886)


def create_random(a, b):
    geod = Geodesic.WGS84
    data = []
    for i in range(360):
        angle = random.randint(0, 360)
        angle1 = random.randint(0, 360)
        pointa = geod.Direct(a[0], a[1], angle, 0.05)
        pointb = geod.Direct(b[0], b[1], angle1, 0.05)
        inv = geod.Inverse(pointa['lat2'], pointa['lon2'], pointb['lat2'], pointb['lon2'])
        angle_ = inv['azi1']
        data.append(angle_)
        #print(angle_)
    print(max(data), min(data))


if __name__ == '__main__':
    geod = Geodesic.WGS84
    point = geod.Direct(pointA[0], pointA[1], 90, 0.8)
    pointB = [point['lat2'], point['lon2']]
    create_random(pointA, pointB)