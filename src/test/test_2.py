
from geographiclib.geodesic import Geodesic
geod = Geodesic.WGS84

pointAlat = 17.5192601 
pointAlon = 78.2736114
angle = 90.0
distance_ = 15.0
spacing = distance_ / 3

for steps in range(int(distance_ / spacing)):
        print((spacing * (steps + 1)))
        point_ = geod.Direct(pointAlat, pointAlon, angle, (spacing * (steps + 1)))