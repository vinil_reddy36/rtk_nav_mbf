<!DOCTYPE HTML>
<html>
    
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="http://192.168.7.6/gps/leaflet.css"/>
    <script src="http://192.168.7.6/gps/leaflet.js"></script>
<title>OpenLayers Simplest Example</title>
<div id="map"></div>
    17.519116 , 78.273261
    <div>
        <br>
        <input type="text" id="gps" value="gps" size="130">
        <br>
        <input type="text" id="t265" value="t265" size="130">
        <br>
        <input type="text" id="magnetometer" value="magnetometer" size="130">
        <br>
        <input type="text" id="navigation" value="navigation" size="130">
        <br>
        <input type="text" id="ultrasonic" value="ultrasonic" size="130">
    </div>
    <button class="btn_c" style="position: absolute; top: 00%; left: 45%;" id="up" onmousedown="send_unpause(false)"
                onmouseup="unclicked()" ontouchstart="send_unpause(false)" ontouchend="unclicked()">continue</button>
    <div style="position: absolute; top: 5%; left: 75%; height: 100%; width: 20%; border: 1px solid #010c08;">
        <div style="position: relative; top: 0%; border: 1px solid #010c08; height: 49%;">
                <button class="btn_" style="position: absolute; top: 00%; left: 45%;" id="up" onmousedown="clicked('up')"
                onmouseup="unclicked('up')" ontouchstart="clicked('up')" ontouchend="unclicked('up')">up</button>
                <button class="btn_" style="position: absolute; top: 70%; left: 43%;" id="down" onmousedown="clicked('down')"
                onmouseup="unclicked('down')" ontouchstart="clicked('down')" ontouchend="unclicked('down')">down</button>
                <button class="btn_" style="position: absolute; top: 35%; left: 10%;" id="left" onmousedown="clicked('left')"
                onmouseup="unclicked('left')" ontouchstart="clicked('left')" ontouchend="unclicked('left')">left</button>
                <button class="btn_" style="position: absolute; top: 35%; left: 75%;" id="right" onmousedown="clicked('right')"
                onmouseup="unclicked('right')" ontouchstart="clicked('right')" ontouchend="unclicked('right')">right</button>
        </div>



        
    <div style="position: absolute; top: 50%; border: 1px solid #010c08; width: 100%;">
                <div class="slidecontainer">
                    <input type="range" min="0.1" max="0.5" value="0.3" step="0.1" class="slider" id="slider_x">
                    <p>Value: <span id="value_x"></span></p>
                </div>

                <div class="slidecontainer">
                    <input type="range" min="0.5" max="1.5" value="1.0" step="0.5" class="slider" id="slider_z">
                    <p>Value: <span id="value_z"></span></p>
                </div>
        </div>
    </div>



    <div style="position: absolute; top: 1000px;">
        <button id="swing_element" class="swing" type="button" onclick="swing()" touchstart="swing()">Swing</button>
        <button id="static_element" class="static" type="button" onclick="static()" touchstart="static()">Static</button>

        <div class="wrapper">
        <div class="values">
            <span id="range-1-1">
                0
            </span>
            <span> &dash; </span>
            <span id="range-1-2">
                100
            </span>
        </div>
        <div class="container">
            <div class="slider-track1"></div>
            <input type="range" min="0" max="180" value="50" id="slider-1-1" oninput="slideOneOne()">
            <input type="range" min="0" max="180" value="120" id="slider-1-2" oninput="slideOneTwo()">
        </div>
        </div>

        <div class="wrapper_2">
        <div class="values2">
            <span id="range-2-1">
                0
            </span>
            <span> &dash; </span>
            <span id="range-2-2">
                100
            </span>
        </div>
        <div class="container">
            <div class="slider-track2"></div>
            <input type="range" min="0" max="180" value="50" id="slider-2-1" oninput="slideTwoOne()">
            <input type="range" min="0" max="180" value="120" id="slider-2-2" oninput="slideTwoTwo()">
        </div>
        </div>

        <div class="wrapper_3">
        <div class="values3">
            <span id="range-3-1">
                0
            </span>
            <span> &dash; </span>
            <span id="range-3-2">
                100
            </span>
        </div>
        <div class="container">
            <div class="slider-track3"></div>
            <input type="range" min="0" max="255" value="0" id="slider-3-1">
            <input type="range" min="0" max="255" value="150" id="slider-3-2" oninput="slide3Two()">
        </div>
        </div>


        <div class="wrapper_4">
        <div class="values4">
            <span id="range-4-1">
                0
            </span>
            <span> &dash; </span>
            <span id="range-4-2">
                100
            </span>
        </div>
        <div class="container">
            <div class="slider-track4"></div>
            <input type="range" min="0" max="180" value="0" id="slider-4-1">
            <input type="range" min="0" max="180" value="20" id="slider-4-2" oninput="slide4Two()">
        </div>
        </div>


        <div class="wrapper_5">
        <div class="values5">
            <span id="range-5-1">
                0
            </span>
            <span> &dash; </span>
            <span id="range-5-2">
                100
            </span>
        </div>
        <div class="container">
            <div class="slider-track5"></div>
            <input type="range" min="0" max="180" value="0" id="slider-5-1">
            <input type="range" min="0" max="180" value="20" id="slider-5-2" oninput="slide5Two()">
        </div>
        </div>
        <p class="left_servo"> left_servo </p>
        <p class="right_servo"> right_servo </p>
        <p class="motor_speed"> motor_speed </p>
    </div>
</head>
<body>
<script src="buttons.js"></script> 
 <script src="slider.js"></script>
<script type="text/javascript" src="http://192.168.7.6/eventemitter2.min.js"></script>
<script type="text/javascript" src="http://192.168.7.6/roslib.min.js"></script>
<script type="text/javascript" src="http://192.168.7.6/gps/MovingMarker.js"></script>

<script src="jquery.min.js"></script>

<Script type="text/javascript">
                    var ip = "192.168.7.6";
                    var connected = true;
                    var image = true;
                    var gps_recieved = false;
                    var lat = 0.0;
                    var lon = 0.0;
                    var line = "";
                    var map_;
                    var ros = new ROSLIB.Ros({
                        url : 'ws://'+ip+':9090'
                    });

                       ros.on('connection', function() {
                            console.log('Connected to websocket server.');
                       });

                       ros.on('error', function(error) {
                            console.log('Error connecting to websocket server: ', error);

                       });

                       ros.on('close', function() {
                            console.log('Connection to websocket server closed.');

                       });

                       var listener = new ROSLIB.Topic({
                        ros : ros,
                        name : '/fix',
                        messageType : 'sensor_msgs/NavSatFix'
                      });

                      var video_listener = new ROSLIB.Topic({
                        ros : ros,
                        name : 'xmachines/camera',
                        messageType : 'sensor_msgs/Image'
                      });
                      video_listener.subscribe(function(message) {
                        if (image){
                        show_image("//"+ip+":8080/stream?topic=/xmachines/camera");
                        image = false;
                        }
                        video_listener.unsubscribe();
                      });

                      function show_image(src) {
                        var img = document.createElement("img");
                        img.src = src;
                        img.width = 640;
                        img.height = 480;

                        // This next line will just add it to the <body> tag
                        document.body.appendChild(img);
                        }
                      
                      listener.subscribe(function(message) {
                        lat = message.latitude;
                        lon = message.longitude;
                        
                        if(!connected){
                            if(!gps_recieved){
                            send_to_database();
                            gps_recieved = true;
                            }
                        }
                        display(lat, lon);
                        marker.setLatLng([lat, lon]).update();
                        update_green_path();
                      });

                      var path = new ROSLIB.Topic({
                        ros : ros,
                        name : '/global_plan/gps_ui',
                        messageType : 'nav_msgs/Path'
                      });

                      path.subscribe(function(message) {
                        display_path(message);
                        
                      });
                    function wait(ms){
                        var start = new Date().getTime();
                        var end = start;
                        while(end < start + ms) {
                            end = new Date().getTime();
                        }
                    }

                    function update_green_path(){
                        var d = "get";
                        $.ajax({
                            url: 'gps_ui.php',
                            type: 'post',
                            data: { "update_pos": d },
                            success: function (response) {
                                        var line_ = response;
                                        if (line_ != "0 results"){
                                        //console.log(line_);
                                            display_green_path(line_.split("/n"))
                                        }
                                
                                    }});
                    }
                    function send_to_database() {
                        var d = "get_list";
                        $.ajax({
                            url: 'gps_ui.php',
                            type: 'post',
                            data: { "get_data": d },
                            success: function (response) {
                                        var line_ = response.split("/n");
                                        console.log(line_);
                                        //wait(3000);
                                        display(lat, lon);
                                        display_path(line_);
                                        console.log("test3");
                                        line = L.polyline([], {
                                                color: 'green',
                                                weight: 9,
                                                opacity: 1,
                                                smoothFactor: 1
                                                }).addTo(map_);
                                
                                    }});
                    }                
                    function display(lat, lon){
                        if(connected){
                            map_ = L.map('map').setView([lat , lon], 19);
                            
                            L.tileLayer('OSMPublicTransport/{z}/{x}/{y}.png', {
                                minZoom: 4,
                                maxZoom: 22,

                                maxNativeZoom: 16,
                                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
                            }).addTo(map_);
                            marker = L.marker(map_.getCenter()).addTo(map_)
                            connected = false;
                        }
                    }
                    function display_green_path(points){
                        display_points = []
                        for (let i = 0; i < points.length - 1; i++){
                            var test = points[i].split(" ");
                            if(test[2] != -0.03 && test[2] != 0.03 && test[2] != -0.04 && test[2] != 0.04){
                                display_points.push([parseFloat(test[0]), parseFloat(test[1])])
                            }
                        }
                        var firstpolyline = new L.Polyline(display_points, {
                                                            color: 'green',
                                                            weight: 3,
                                                            opacity: 0.5,
                                                            smoothFactor: 1
                                                            });
                        firstpolyline.addTo(map_);
                    }
                    function display_path(points){

                        display_points = []
                        for (let i = 0; i < points.length - 1; i++){
                            var test = points[i].split(" ");
                            if(test[2] != -0.03 && test[2] != 0.03 && test[2] != -0.04 && test[2] != 0.04){
                                display_points.push([parseFloat(test[0]), parseFloat(test[1])])
                            }
                        }
                        var firstpolyline = new L.Polyline(display_points, {
                                                            color: 'red',
                                                            weight: 3,
                                                            opacity: 0.5,
                                                            smoothFactor: 1
                                                            });
                        console.log("test");
                        console.log(map_);
                        firstpolyline.addTo(map_);
                        console.log("test2");

                    }

                    


</Script>
<script type="text/javascript" src="http://192.168.7.6/gps/joy/joy_stick.js"></script>
</body>

</html>