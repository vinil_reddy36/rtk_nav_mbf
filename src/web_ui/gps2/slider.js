
        window.onload = function(){
            slideOneOne();
            slideOneTwo();
            slideTwoOne();
            slideTwoTwo();
            slide3One();
            slide3Two();
            slide4One();
            slide4Two();
            slide5One();
            slide5Two();
            
        }

        let sliderOneOne = document.getElementById("slider-1-1");
        let sliderOneTwo = document.getElementById("slider-1-2");
        let displayValOneOne = document.getElementById("range-1-1");
        let displayValOneTwo = document.getElementById("range-1-2");
        let minGap = 0;
        let sliderTrackOne = document.querySelector(".slider-track1");
        let sliderMaxValueOne = document.getElementById("slider-1-1").max;

        function slideOneOne(){
            if(parseInt(sliderOneTwo.value) - parseInt(sliderOneOne.value) <= minGap){
                sliderOneOne.value = parseInt(sliderOneTwo.value) - minGap;
            }
            displayValOneOne.textContent = sliderOneOne.value;
            fillColor_One();
        }
        function slideOneTwo(){
            if(parseInt(sliderOneTwo.value) - parseInt(sliderOneOne.value) <= minGap){
                sliderOneTwo.value = parseInt(sliderOneOne.value) + minGap;
            }
            displayValOneTwo.textContent = sliderOneTwo.value;
            fillColor_One();
        }
        function fillColor_One(){
            percent1 = (sliderOneOne.value / sliderMaxValueOne) * 100;
            percent2 = (sliderOneTwo.value / sliderMaxValueOne) * 100;
            sliderTrackOne.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #3264fe ${percent1}% , #3264fe ${percent2}%, #dadae5 ${percent2}%)`;
        }


        let sliderTwoOne = document.getElementById("slider-2-1");
        let sliderTwoTwo = document.getElementById("slider-2-2");
        let displayValTwoOne = document.getElementById("range-2-1");
        let displayValTwoTwo = document.getElementById("range-2-2");
        let sliderTrackTwo = document.querySelector(".slider-track2");
        let sliderMaxValueTwo = document.getElementById("slider-2-1").max;

        function slideTwoOne(){
            if(parseInt(sliderTwoTwo.value) - parseInt(sliderTwoOne.value) <= minGap){
                sliderTwoOne.value = parseInt(sliderTwoTwo.value) - minGap;
            }
            displayValTwoOne.textContent = sliderTwoOne.value;
            fillColor();
        }
        function slideTwoTwo(){
            if(parseInt(sliderTwoTwo.value) - parseInt(sliderTwoOne.value) <= minGap){
                sliderTwoTwo.value = parseInt(sliderTwoOne.value) + minGap;
            }
            displayValTwoTwo.textContent = sliderTwoTwo.value;
            fillColor();
        }
        function fillColor(){
            percent1 = (sliderTwoOne.value / sliderMaxValueTwo) * 100;
            percent2 = (sliderTwoTwo.value / sliderMaxValueTwo) * 100;
            sliderTrackTwo.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #3264fe ${percent1}% , #3264fe ${percent2}%, #dadae5 ${percent2}%)`;
        }


        let slider3One = document.getElementById("slider-3-1");
        let slider3Two = document.getElementById("slider-3-2");
        let displayVal3One = document.getElementById("range-3-1");
        let displayVal3Two = document.getElementById("range-3-2");
        let sliderTrack3 = document.querySelector(".slider-track3");
        let sliderMaxValue3 = document.getElementById("slider-3-1").max;

        function slide3One(){
            if(parseInt(slider3Two.value) - parseInt(slider3One.value) <= minGap){
                slider3One.value = parseInt(slider3Two.value) - minGap;
            }
            displayVal3One.textContent = slider3One.value;
            fillColor3();
        }
        function slide3Two(){
            if(parseInt(slider3Two.value) - parseInt(slider3One.value) <= minGap){
                slider3Two.value = parseInt(slider3One.value) + minGap;
            }
            send_pump_speed();
            displayVal3Two.textContent = slider3Two.value;
            fillColor3();
        }

        function fillColor3(){
            percent1 = (slider3One.value / sliderMaxValue3) * 100;
            percent2 = (slider3Two.value / sliderMaxValue3) * 100;
            sliderTrack3.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #3264fe ${percent1}% , #3264fe ${percent2}%, #dadae5 ${percent2}%)`;
        }

        let slider4One = document.getElementById("slider-4-1");
        let slider4Two = document.getElementById("slider-4-2");
        let displayVal4One = document.getElementById("range-4-1");
        let displayVal4Two = document.getElementById("range-4-2");
        let sliderTrack4 = document.querySelector(".slider-track4");
        let sliderMaxValue4 = document.getElementById("slider-4-1").max;

        function slide4One(){
            if(parseInt(slider4Two.value) - parseInt(slider4One.value) <= minGap){
                slider4One.value = parseInt(slider4Two.value) - minGap;
            }
            displayVal4One.textContent = slider4One.value;
            fillColor4();
        }
        function slide4Two(){
            if(parseInt(slider4Two.value) - parseInt(slider4One.value) <= minGap){
                slider4Two.value = parseInt(slider4One.value) + minGap;
            }
            send_left_motor(parseInt(slider4Two.value));
            displayVal4Two.textContent = slider4Two.value;
            fillColor4();
        }
        
        function fillColor4(){
            percent1 = (slider4One.value / sliderMaxValue4) * 100;
            percent2 = (slider4Two.value / sliderMaxValue4) * 100;
            sliderTrack4.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #4264fe ${percent1}% , #4264fe ${percent2}%, #dadae5 ${percent2}%)`;
        }


        let slider5One = document.getElementById("slider-5-1");
        let slider5Two = document.getElementById("slider-5-2");
        let displayVal5One = document.getElementById("range-5-1");
        let displayVal5Two = document.getElementById("range-5-2");
        let sliderTrack5 = document.querySelector(".slider-track5");
        let sliderMaxValue5 = document.getElementById("slider-5-1").max;

        function slide5One(){
            if(parseInt(slider5Two.value) - parseInt(slider5One.value) <= minGap){
                slider5One.value = parseInt(slider5Two.value) - minGap;
            }
            displayVal5One.textContent = slider5One.value;
            fillColor5();
        }
        function slide5Two(){
            if(parseInt(slider5Two.value) - parseInt(slider5One.value) <= minGap){
                slider5Two.value = parseInt(slider5One.value) + minGap;
            }
            send_right_motor(parseInt(slider5Two.value));
            displayVal5Two.textContent = slider5Two.value;
            fillColor5();
        }
        
        function fillColor5(){
            percent1 = (slider5One.value / sliderMaxValue5) * 100;
            percent2 = (slider5Two.value / sliderMaxValue5) * 100;
            sliderTrack5.style.background = `linear-gradient(to right, #dadae5 ${percent1}% , #5265fe ${percent1}% , #5265fe ${percent2}%, #dadae5 ${percent2}%)`;
        }

        