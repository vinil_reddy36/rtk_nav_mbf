var swing_ = false;
function swing(){
    console.log("Pressed swing");
    var swing_element = document.getElementById("swing_element");
    if(swing_ === false){
        swing_ = true;
        send_swing_state(swing_);
        toggle(swing_element, swing_);
        console.log(swing_);
    }
    else{
        swing_ = false;
        send_swing_state(swing_);
        toggle(swing_element, swing_);
        console.log(swing_);
    }
}

var static_ = false;
function static(){
    console.log("Pressed static");
    var static_element = document.getElementById("static_element");
    if(static_ === false){
        static_ = true;
        send_static_state(static_);
        toggle(static_element, static_);
        console.log(static_);
    }
    else{
        static_ = false;
        send_static_state(static_);
        console.log(static_);
        toggle(static_element, static_);
    }
}

function toggle(button, clicked){
    if (clicked == true ) {
      button.style.backgroundColor = "green";
      button.style.transform = "translateY(10px)";
      button.style.boxShadow = "0px 9px #666";
    } else {

      button.style.backgroundColor = "red";
      button.style.transform = "translateY(2px)";
      button.style.boxShadow = "0px 15px #666";
    }
}


function send_pump_speed(){
        var send_pump_speed_ = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/pump_speed',
            messageType : 'std_msgs/Int16'
        });
        var pump_speed = new ROSLIB.Message({
            data : parseInt(slider3Two.value),
        });
        send_pump_speed_.publish(pump_speed);
    }

    function send_swing_state(data){
        send_motor_angles();
        var swing_state = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/swing',
            messageType : 'std_msgs/Bool'
          });
          
         var state = new ROSLIB.Message({
            data : data,
          });
          swing_state.publish(state);
    }
    
    function send_static_state(data){
        var static_state = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/static',
            messageType : 'std_msgs/Bool'
          });
          
         var state = new ROSLIB.Message({
            data : data,
          });
          static_state.publish(state);
    }

    function send_motor_angles(){
        var send_motor_angles_ = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/motor_angles',
            messageType : 'std_msgs/Int16MultiArray'
        });
        var angles = new ROSLIB.Message({
            data : [parseInt(sliderOneOne.value), 
                    parseInt(sliderOneTwo.value), 
                    parseInt(sliderTwoOne.value),
                    parseInt(sliderTwoTwo.value)
                ],
        });
        send_motor_angles_.publish(angles);
    }

    function send_left_motor(speed){
        var send_left_motor_speed = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/servo_angle_left',
            messageType : 'std_msgs/Int16'
        });
        var speed_ = new ROSLIB.Message({
            data : speed
        });
        send_left_motor_speed.publish(speed_);
    }

    function send_right_motor(speed){
        var send_right_motor_speed = new ROSLIB.Topic({
            ros : ros,
            name : '/ui/servo_angle_right',
            messageType : 'std_msgs/Int16'
        });
        var speed_ = new ROSLIB.Message({
            data : speed
        });
        send_right_motor_speed.publish(speed_);
    }