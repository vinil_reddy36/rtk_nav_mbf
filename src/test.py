#!/usr/bin/env python
import rospy
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from geodesy import utm
from geographiclib.geodesic import Geodesic


pointA = (17.5190915, 78.2733940)
pointB = (17.5191036, 78.2733886)

if __name__ == '__main__':
    rospy.init_node("test")
    geod = Geodesic.WGS84
    inv = geod.Inverse(pointA[0], pointA[1], pointB[0], pointB[1])
    angle = inv['azi1']
    print(angle)
