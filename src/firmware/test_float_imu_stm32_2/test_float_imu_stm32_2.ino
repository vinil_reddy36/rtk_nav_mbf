String inputString = ""; // string to hold incoming data
bool readData = false;
float values[27];

void setup() {
  Serial.begin(57600);
  Serial.setTimeout(25);
}

void loop() {
  while (Serial.available() > 0) {
    char c = Serial.read();
    if (c == '\n') { // end of input string
      if (inputString == "ok") {
        readData = true; // set flag to start reading data
      } else if (readData) {
        int i = 0;
        char inputChars[inputString.length() + 1];
        strcpy(inputChars, inputString.c_str());
        char *tok = strtok(inputChars, " ");
        while (tok != NULL && i < 27) {
          values[i++] = atof(tok);
          tok = strtok(NULL, " ");
        }
        if (i == 27) {
          // do something with the values array here
          // for example, print them out:
          for (int j = 0; j < 27; j++) {
            Serial.print(values[j]);
            Serial.print(" ");
          }
          Serial.println();
        } else {
          Serial.println("Invalid number of values received.");
        }
        readData = false; // reset flag
      }
      inputString = ""; // clear input string
    } else {
      inputString += c; // add character to input string
    }
  }
}
