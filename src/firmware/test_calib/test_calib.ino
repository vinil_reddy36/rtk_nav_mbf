#include "Wire.h"
#include "/home/vinil/MPU-9250-AHRS-master/libs/I2Cdev.cpp"
#include "/home/vinil/MPU-9250-AHRS-master/libs/MPU9250.cpp"
MPU9250 imu_c;
I2Cdev   I2C_M;
#define sample_num  10
uint8_t buffer_m[6];  
int16_t ax_c, ay_c, az_c;
int16_t gx_c, gy_c, gz_c;
int16_t mx_c, my_c, mz_c;
float Axyz_c[3] = {0};
float Gxyz_c[3] = {0};
float Mxyz_c[3] = {0};
float A_cal[6] = {0}; 
float M_cal[6] = {0};
int N_c = sample_num;



//MPU9250 accelgyro;

float A_B[3];
float A_Ainv[3][3];
float M_B[3];
float M_Ainv[3][3];
float G_off[3];
int16_t ax, ay, az;
int16_t gx, gy, gz;
int16_t mx, my, mz;
float Axyz[3];
float Gxyz[3];
float Mxyz[3];
#define gscale (250./32768.0)*(PI/180.0)  //gyro default 250 LSB per d/s -> rad/s
#define Kp 10.0
#define Ki 0.0
unsigned long now = 0, last = 0; //micros() timers
float deltat = 0;  //loop time in seconds
unsigned long now_ms, last_ms = 0; //millis() timers
unsigned long print_ms = 100; //print every "print_ms" milliseconds


static float q[4] = {1.0, 0.0, 0.0, 0.0};
static float yaw, pitch, roll; //Euler angle output
bool done = false;


void setup() {
  Wire.begin();
  Serial.begin(57600);
  
}
String decision;
bool recieved = true;
void loop()
{
  if(Serial.available()>0 && recieved == true){
    String data = Serial.readString();
    decision = data.substring(0,5);
    if(decision == "ok"){
      recieved = false;
    }
  }
  if(decision == "ok"){
    Serial.println("Initializing I2C devices...");
  imu_c.initialize();
  Serial.println("Testing device connections...");
  Serial.println(imu_c.testConnection() ? "MPU9250 OK" : "MPU9250 ??");
  Serial.println("Gyro bias collection ... KEEP SENSOR STILL");
  delay(2000);
  for (int i = 0; i < N_c; i++) {
    imu_c.getMotion9(&ax_c, &ay_c, &az_c, &gx_c, &gy_c, &gz_c, &mx_c, &my_c, &mz_c);
    Gxyz_c[0] += float(gx_c);
    Gxyz_c[1] += float(gy_c);
    Gxyz_c[2] += float(gz_c);
  }
  Serial.print("Done. Gyro offsets (raw) ");
  Serial.print(Gxyz_c[0] / N_c, 1);
  Serial.print(", ");
  Serial.print(Gxyz_c[1] / N_c, 1);
  Serial.print(", ");
  Serial.print(Gxyz_c[2] / N_c, 1);
  Serial.println();

  Serial.print("Collecting ");
  Serial.print(N_c);
  Serial.println(" points for scaling,3/second");
  Serial.println("TURN SENSOR VERY SLOWLY AND CAREFULLY IN 3D");
  delay(2000);

  float M_mag = 0, A_mag = 0;
  int i, j;
  j = N_c;
  while (j-- >= 0) {
    getAcc_Mag_raw();
    for (i = 0; i < 3; i++) {
      M_mag += Mxyz_c[i] * Mxyz_c[i];
      A_mag += Axyz_c[i] * Axyz_c[i];
    }
    Serial.print(ax_c);
    Serial.print(" ");
    Serial.print(ay_c);
    Serial.print(" ");
    Serial.print(az_c);
    Serial.print(" ");
    Serial.print(mx_c);
    Serial.print(" ");
    Serial.print(my_c);
    Serial.print(" ");
    Serial.println(mz_c);
    delay(300);
  }
  Serial.print("Done. ");
  Serial.print("rms Acc = ");
  Serial.print(sqrt(A_mag / N_c));
  Serial.print(", rms Mag = ");
  Serial.println(sqrt(M_mag / N_c));
  decision = "okk";
  Serial.println("over");
  }

  if(decision == "okk"){
    //Serial.println("okk_");
    if (Serial.available() > 1 && done == false) {
      String floatStr = Serial.readString(); // Read the long string of floats
      if (floatStr == "over"){
        done = true;
      }
      if(done == false){
        float floatArray[27]; // Create an array to store the 27 float values
        int i = 0;
        char *ptr = strtok((char*)floatStr.c_str(), " "); // Parse the long string into individual floats
        while (ptr != NULL && i < 27) {
          floatArray[i] = atof(ptr); // Convert the string to a float and store in the array
          ptr = strtok(NULL, " ");
          i++;
        }
  
        
        A_B[0] = floatArray[0];
        A_B[1] = floatArray[1];
        A_B[2] = floatArray[2];
        A_Ainv[0][0] = floatArray[3];
        A_Ainv[0][1] = floatArray[4];
        A_Ainv[0][2] = floatArray[5];
        A_Ainv[1][0] = floatArray[6];
        A_Ainv[1][1] = floatArray[7];
        A_Ainv[1][2] = floatArray[8];
        A_Ainv[2][0] = floatArray[9];
        A_Ainv[2][1] = floatArray[10];
        A_Ainv[2][2] = floatArray[11];
        M_B[0] = floatArray[12];
        M_B[1] = floatArray[13];
        M_B[2] = floatArray[14];
        M_Ainv[0][1] = floatArray[15];
        M_Ainv[0][2] = floatArray[16];
        M_Ainv[0][3] = floatArray[17];
        M_Ainv[1][1] = floatArray[18];
        M_Ainv[1][2] = floatArray[19];
        M_Ainv[1][3] = floatArray[20];
        M_Ainv[2][1] = floatArray[21];
        M_Ainv[2][2] = floatArray[22];
        M_Ainv[2][3] = floatArray[23];
        G_off[0] = floatArray[24];
        G_off[1] = floatArray[25];
        G_off[2] = floatArray[26];
        // Print the 27 float values to the serial monitor
        for (int j = 0; j < 27; j++) {
          Serial.print(floatArray[j], 5);
          Serial.print(" ");
        }
        Serial.println();
    }
    }
    if(done){
      
      get_MPU_scaled();
      now = micros();
      deltat = (now - last) * 1.0e-6; //seconds since last update
      last = now;
    
      // correct for differing accelerometer and magnetometer alignment by circularly permuting mag axes
    
      MahonyQuaternionUpdate(Axyz[0], Axyz[1], Axyz[2], Gxyz[0], Gxyz[1], Gxyz[2],
                             Mxyz[1], Mxyz[0], -Mxyz[2], deltat);
      roll  = atan2((q[0] * q[1] + q[2] * q[3]), 0.5 - (q[1] * q[1] + q[2] * q[2]));
      pitch = asin(2.0 * (q[0] * q[2] - q[1] * q[3]));
      yaw   = atan2((q[1] * q[2] + q[0] * q[3]), 0.5 - ( q[2] * q[2] + q[3] * q[3]));
      // to degrees
      yaw   *= 180.0 / PI;
      pitch *= 180.0 / PI;
      roll *= 180.0 / PI;
      Serial.println(yaw);
      delay(10);
  }
  }
  
}

void getAcc_Mag_raw(void)
{
  imu_c.getMotion9(&ax_c, &ay_c, &az_c, &gx_c, &gy_c, &gz_c, &mx_c, &my_c, &mz_c);
  Axyz_c[0] = (float) ax_c;
  Axyz_c[1] = (float) ay_c;
  Axyz_c[2] = (float) az_c;
  Mxyz_c[0] = (float) mx_c;
  Mxyz_c[1] = (float) my_c;
  Mxyz_c[2] = (float) mz_c;
}

void getGyro_scaled(void)
{
  imu_c.getMotion9(&ax_c, &ay_c, &az_c, &gx_c, &gy_c, &gz_c, &mx_c, &my_c, &mz_c);
  Gxyz_c[0] = (float) gx_c * 250 / 32768;//250 LSB(d/s)
  Gxyz_c[1] = (float) gy_c * 250 / 32768;
  Gxyz_c[2] = (float) gz_c * 250 / 32768;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void get_MPU_scaled(void) {
  float temp[3];
  int i;
  imu_c.getMotion9(&ax, &ay, &az, &gx, &gy, &gz, &mx, &my, &mz);

  Gxyz[0] = ((float) gx - G_off[0]) * gscale; //250 LSB(d/s) default to radians/s
  Gxyz[1] = ((float) gy - G_off[1]) * gscale;
  Gxyz[2] = ((float) gz - G_off[2]) * gscale;

  Axyz[0] = (float) ax;
  Axyz[1] = (float) ay;
  Axyz[2] = (float) az;
  //apply offsets (bias) and scale factors from Magneto
  for (i = 0; i < 3; i++) temp[i] = (Axyz[i] - A_B[i]);
  Axyz[0] = A_Ainv[0][0] * temp[0] + A_Ainv[0][1] * temp[1] + A_Ainv[0][2] * temp[2];
  Axyz[1] = A_Ainv[1][0] * temp[0] + A_Ainv[1][1] * temp[1] + A_Ainv[1][2] * temp[2];
  Axyz[2] = A_Ainv[2][0] * temp[0] + A_Ainv[2][1] * temp[1] + A_Ainv[2][2] * temp[2];
  vector_normalize(Axyz);

  Mxyz[0] = (float) mx;
  Mxyz[1] = (float) my;
  Mxyz[2] = (float) mz;
  //apply offsets and scale factors from Magneto
  for (i = 0; i < 3; i++) temp[i] = (Mxyz[i] - M_B[i]);
  Mxyz[0] = M_Ainv[0][0] * temp[0] + M_Ainv[0][1] * temp[1] + M_Ainv[0][2] * temp[2];
  Mxyz[1] = M_Ainv[1][0] * temp[0] + M_Ainv[1][1] * temp[1] + M_Ainv[1][2] * temp[2];
  Mxyz[2] = M_Ainv[2][0] * temp[0] + M_Ainv[2][1] * temp[1] + M_Ainv[2][2] * temp[2];
  vector_normalize(Mxyz);
 }

// Mahony scheme uses proportional and integral filtering on
// the error between estimated reference vectors and measured ones.
void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz, float deltat)
{
  // Vector to hold integral error for Mahony method
  static float eInt[3] = {0.0, 0.0, 0.0};
  // short name local variable for readability
  float q1 = q[0], q2 = q[1], q3 = q[2], q4 = q[3];
  float norm;
  float hx, hy, bx, bz;
  float vx, vy, vz, wx, wy, wz;
  float ex, ey, ez;
  float pa, pb, pc;

  // Auxiliary variables to avoid repeated arithmetic
  float q1q1 = q1 * q1;
  float q1q2 = q1 * q2;
  float q1q3 = q1 * q3;
  float q1q4 = q1 * q4;
  float q2q2 = q2 * q2;
  float q2q3 = q2 * q3;
  float q2q4 = q2 * q4;
  float q3q3 = q3 * q3;
  float q3q4 = q3 * q4;
  float q4q4 = q4 * q4;
  /*
    // already done in loop()

    // Normalise accelerometer measurement
    norm = sqrt(ax * ax + ay * ay + az * az);
    if (norm == 0.0f) return; // Handle NaN
    norm = 1.0f / norm;       // Use reciprocal for division
    ax *= norm;
    ay *= norm;
    az *= norm;

    // Normalise magnetometer measurement
    norm = sqrt(mx * mx + my * my + mz * mz);
    if (norm == 0.0f) return; // Handle NaN
    norm = 1.0f / norm;       // Use reciprocal for division
    mx *= norm;
    my *= norm;
    mz *= norm;
  */
  // Reference direction of Earth's magnetic field
  hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
  hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
  bx = sqrt((hx * hx) + (hy * hy));
  bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

  // Estimated direction of gravity and magnetic field
  vx = 2.0f * (q2q4 - q1q3);
  vy = 2.0f * (q1q2 + q3q4);
  vz = q1q1 - q2q2 - q3q3 + q4q4;
  wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
  wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
  wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

  // Error is cross product between estimated direction and measured direction of the reference vectors
  ex = (ay * vz - az * vy) + (my * wz - mz * wy);
  ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
  ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
  if (Ki > 0.0f)
  {
    eInt[0] += ex;      // accumulate integral error
    eInt[1] += ey;
    eInt[2] += ez;
    // Apply I feedback
    gx += Ki*eInt[0];
    gy += Ki*eInt[1];
    gz += Ki*eInt[2];
  }


  // Apply P feedback
  gx = gx + Kp * ex; 
  gy = gy + Kp * ey;
  gz = gz + Kp * ez;

// Integrate rate of change of quaternion
 // small correction 1/11/2022, see https://github.com/kriswiner/MPU9250/issues/447
gx = gx * (0.5*deltat); // pre-multiply common factors
gy = gy * (0.5*deltat);
gz = gz * (0.5*deltat);
float qa = q1;
float qb = q2;
float qc = q3;
q1 += (-qb * gx - qc * gy - q4 * gz);
q2 += (qa * gx + qc * gz - q4 * gy);
q3 += (qa * gy - qb * gz + q4 * gx);
q4 += (qa * gz + qb * gy - qc * gx);

  // Normalise quaternion
  norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
  norm = 1.0f / norm;
  q[0] = q1 * norm;
  q[1] = q2 * norm;
  q[2] = q3 * norm;
  q[3] = q4 * norm;
}

float vector_dot(float a[3], float b[3])
{
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

void vector_normalize(float a[3])
{
  float mag = sqrt(vector_dot(a, a));
  a[0] /= mag;
  a[1] /= mag;
  a[2] /= mag;
}
