#!/usr/bin/env python

# This generates global paths and sends it to move_base_flex.
# code tested on simulation (husky_waypoint_navigation)
# result : working



import math
import time
import rospy
import actionlib
from geodesy import utm
from nav_msgs.msg import Path, Odometry
from geometry_msgs.msg import PoseStamped, PointStamped
from mbf_msgs.msg import ExePathGoal
import mbf_msgs.msg as mbf_msgs
import tf.listener
from tf import transformations
from sensor_msgs.msg import NavSatFix
from geographiclib.geodesic import Geodesic

global current_lat, current_lon
current_lat = 0.0
current_lon = 0.0


A = (49.9000000, 8.9000000)
B = (49.9000396, 8.9000000)
C = (49.9000407, 8.8999313)
D = (49.8999996, 8.8999294)
row_space = 2.0 # in meter
number_of_rows = 2


def generate_path(poses, angle):
    path_ = Path()
    q = transformations.quaternion_from_euler(0, 0, math.radians(angle))
    count = 0
    for x, y in poses:
        count = count + 1
        if count < len(poses):
            pose = PoseStamped()
            pose.header.frame_id = "map"
            pose.pose.position.x = x
            pose.pose.position.y = y
            pose.pose.orientation.z = 0
            pose.pose.orientation.w = 1
            pose.header.stamp = rospy.Time.now()
            path_.header.frame_id = "map"
            path_.header.stamp = rospy.Time.now()
            path_.poses.append(pose)
        else:
            pose = PoseStamped()
            pose.header.frame_id = "map"
            pose.pose.position.x = x
            pose.pose.position.y = y
            pose.pose.orientation.x = q[0]
            pose.pose.orientation.y = q[1]
            pose.pose.orientation.z = q[2]
            pose.pose.orientation.w = q[3]
            pose.header.stamp = rospy.Time.now()
            path_.header.frame_id = "map"
            path_.header.stamp = rospy.Time.now()
            path_.poses.append(pose)
    return path_


def get_gps_points(pointAlat, pointAlon, pointBlat, pointBlon, spacing):
    geod = Geodesic.WGS84
    inv = geod.Inverse(pointAlat, pointAlon, pointBlat, pointBlon)
    angle = inv['azi1']
    distance = inv['s12']
    number_of_points = distance / spacing
    gps_points = []
    for i in range(int(number_of_points)):
        point = geod.Direct(pointAlat, pointAlon, angle, (spacing * (i + 1)))
        gps_points.append([point['lat2'], point['lon2']])
    return gps_points


def convert_gps_to_map_points(gps_points):
    map_point = []
    listener = tf.listener.TransformListener()
    Time = rospy.Time(0)
    listener.waitForTransform("/map", "/utm", Time, rospy.Duration(3.0))
    for lat, lon in gps_points:
        UTM_point = PointStamped()
        UTM = utm.fromLatLong(lat, lon).toPoint()
        UTM_point.point.x = UTM.x
        UTM_point.point.y = UTM.y
        UTM_point.header.frame_id = "utm"
        UTM_point.header.stamp = rospy.Time(0)
        UTM_point.header.stamp = Time
        point = listener.transformPoint("map", UTM_point)
        map_point.append([point.point.x, point.point.y])
    return map_point


def get_current_lat_lon(data):
    global current_lat, current_lon
    current_lat = data.latitude
    current_lon = data.longitude


def send_goal(goal_lat, goal_lon, pub_, mbf_ep_ac_, yaw_):
    target_path_ = ExePathGoal()
    global current_lat, current_lon
    latlons = get_gps_points(current_lat, current_lon, goal_lat, goal_lon, 0.02)
    map_points = convert_gps_to_map_points(latlons)
    path = generate_path(map_points, yaw_)
    pub_.publish(path)
    target_path_.path = path
    mbf_ep_ac_.send_goal(target_path_)
    mbf_ep_ac_.wait_for_result()


def get_gps_goal_points(pointA, pointB, pointC, pointD, row_spacing):
    top_points = []
    bottom_points = []
    geod = Geodesic.WGS84
    inv = geod.Inverse(pointB[0], pointB[1], pointC[0], pointC[1])
    angle = inv['azi1']
    distance = inv['s12']
    no_of_rows = distance / row_spacing
    for i in range(int(no_of_rows)):
        point = geod.Direct(pointB[0], pointB[1], angle, (row_spacing*(i+1)))
        top_points.append((point['lat2'], point['lon2']))
    inv = geod.Inverse(pointB[0], pointB[1], pointC[0], pointC[1])
    angle = inv['azi1']
    distance = inv['s12']
    for i in range(int(no_of_rows)):
        point = geod.Direct(pointA[0], pointA[1], angle, (row_spacing * (i+1)))
        bottom_points.append((point['lat2'], point['lon2']))
    print(top_points)
    print(bottom_points)
    return top_points, bottom_points


def get_angle(data):
    q1 = data.pose.pose.orientation.x
    q2 = data.pose.pose.orientation.y
    q3 = data.pose.pose.orientation.z
    q4 = data.pose.pose.orientation.w
    angle = transformations.euler_from_quaternion([q1, q2, q3, q4])
    #print(angle)


if __name__ == '__main__':
    rospy.init_node("single_point_nav", anonymous=True)
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    rospy.Subscriber("/navsat/fix", NavSatFix, get_current_lat_lon)
    rospy.Subscriber("/outdoor_waypoint_nav/odometry/filtered_map", Odometry, get_angle)
    mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    mbf_ep_ac.wait_for_server(rospy.Duration(10))
    rospy.loginfo("Connected to Move Base Flex ExePath server!")
    done = False
    while not rospy.is_shutdown() and done == False:
        print(current_lat, current_lon)
        top, bottom = get_gps_goal_points(A, B, C, D, row_space)
        if current_lat != 0.0:
            send_goal(B[0], B[1], pub, mbf_ep_ac, 0.0)
            send_goal(top[0][0], top[0][1], pub, mbf_ep_ac, 90.0)
    
            for i in range((number_of_rows * 2) - 3):
                if (i+1) % 2 != 0:
                    send_goal(bottom[i][0], bottom[i][1], pub, mbf_ep_ac, 90.0)
                    send_goal(bottom[i+1][0], bottom[i+1][1],pub, mbf_ep_ac, 0.0)
                else:
                    send_goal(top[i][0], top[i][1], pub, mbf_ep_ac, 0.0)
                    send_goal(top[i + 1][0], top[i + 1][1], pub, mbf_ep_ac, 0.0 )
            done = True
        time.sleep(0.01)