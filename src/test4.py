#!/usr/bin/env python

# This generates global paths and sends it to move_base_flex.
# code tested on simulation (husky_waypoint_navigation)
# result : working


import math
import time
import rospy
import actionlib
from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped
from mbf_msgs.msg import ExePathGoal
import mbf_msgs.msg as mbf_msgs


def generate_path(current_x, current_y, goal_x, goal_y, yaw, path_):
    distance = math.sqrt(((goal_y - current_y) ** 2) + ((goal_x - current_x) ** 2))
    diff_x = goal_x - current_x
    diff_y = goal_y - current_y
    point_num = distance / 0.2
    intervel_x = diff_x / point_num
    intervel_y = diff_y / point_num
    for i in range(int(point_num)):
        pose = PoseStamped()
        pose.header.frame_id = "map"
        pose.pose.position.x = current_x + intervel_x * i
        pose.pose.position.y = current_y + intervel_y * i
        pose.pose.orientation.z = 0
        pose.pose.orientation.w = 1
        pose.header.stamp = rospy.Time.now()
        path_.header.frame_id = "map"
        path_.header.stamp = rospy.Time.now()
        path_.poses.append(pose)
    print(path_)
    return path_


if __name__ == '__main__':
    rospy.init_node("test3", anonymous=True)
    pub = rospy.Publisher("/global_plan_proxy", Path, queue_size=10)
    mbf_ep_ac = actionlib.SimpleActionClient("/move_base_flex/exe_path", mbf_msgs.ExePathAction)
    mbf_ep_ac.wait_for_server(rospy.Duration(10))
    rospy.loginfo("Connected to Move Base Flex ExePath server!")
    target_path_ = ExePathGoal()
    path_ = Path()
    pose = PoseStamped()
    posx = 0.0
    posy = 0.0
    gx = 2.0
    gy = 2.0
    yaw = 0
    path__ = generate_path(posx, posy, gx, gy, yaw, path_)
    pub.publish(path__)
    time.sleep(5)
    target_path_.path = path__
    mbf_ep_ac.send_goal(target_path_)
    mbf_ep_ac.wait_for_result()
