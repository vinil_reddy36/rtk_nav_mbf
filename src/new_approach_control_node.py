#!/usr/bin/env python
import time
import rospy
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist, Point
from nav_msgs.msg import Odometry, Path
from tf import transformations
import math
from simple_pid import PID

pid = PID(0.03, 0, 0.015, setpoint=1)
position_x = 0.0
position_y = 0.0
yaw_ = 0.0
path_ = Path()
yaw_precision = math.radians(2.0)
state_ = 0
dist_precision_ = 0.5


def get_odometry(data):
    global position_x, position_y, yaw_
    position_x = data.pose.pose.position.x
    position_y = data.pose.pose.position.y
    quaternion = (data.pose.pose.orientation.x,
                  data.pose.pose.orientation.y,
                  data.pose.pose.orientation.z,
                  data.pose.pose.orientation.w)
    yaw_ = transformations.euler_from_quaternion(quaternion)[2]


def get_path(data):
    global path_
    path_ = data


def calculate_error_yaw(required, present):
    # if 0.0 >= required >= -90.0 and 0.0 < present <= 90.0 \
    #         or (0.0 <= required <= 180.0 and 0.0 <= present <= 180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 >= present >= -180.0) \
    #         or (0.0 >= required >= -180.0 and 0.0 <= present <= 180.0)\
    #         or (0.0 <= required <= 180.0 and 0.0 >= present >= -180.0):
    if required < -90.0 and present > 90.0:
        return 360.0 + (required - present)
    elif required >= 90.0 and present <= -90.0:
        return (required - present) - 360.0
    else:
        return required - present


def go_straight_ahead(point, vel):
    global yaw_, yaw_precision, state_
    twist_msg = Twist()
    err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                        pow(point.pose.position.x - position_x, 2))
    rate = rospy.Rate(20)
    while err_pos > dist_precision_:
        desired_yaw = math.degrees(math.atan2(point.pose.position.y - position_y,
                                 point.pose.position.x - position_x))
        err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                            pow(point.pose.position.x - position_x, 2))
        if point.pose.position.z == 0:
            err_yaw = calculate_error_yaw(desired_yaw, math.degrees(yaw_))
            twist_msg.linear.x = 0.4
            pid.setpoint = 0
            pid.output_limits = (-0.6, 0.6)
            output = pid(err_yaw)
            print(err_yaw, output)
            #   print("output " + str(output))
            twist_msg.angular.z = -output
        else:
            err_yaw = calculate_error_yaw(desired_yaw, math.degrees(yaw_))
            twist_msg.linear.x = -0.4
            pid.setpoint = 0
            pid.output_limits = (-0.6, 0.6)
            output = pid(err_yaw)
            #   print("output " + str(output))
            twist_msg.angular.z = output
        '''
        while math.fabs(err_yaw) > math.radians(45):
            desired_yaw = math.atan2(point.pose.position.y - position_y,
                                 point.pose.position.x - position_x)
            err_yaw = desired_yaw - yaw_
            twist_msg.linear.x = 0.0
            pid.setpoint = desired_yaw
            pid.output_limits = (-0.9, 0.9)
            output = pid(yaw_)
            #   print("output " + str(output))
            twist_msg.angular.z = output
            vel.publish(twist_msg)
            rate.sleep()
        '''
        vel.publish(twist_msg)
        rate.sleep()
    twist_msg.linear.x = 0.0
    twist_msg.angular.z = 0.0
    vel.publish(twist_msg)


if __name__ == '__main__':
    rospy.init_node("local_planner", anonymous=True)
    rospy.Subscriber("/outdoor_waypoint_nav/odometry/filtered_map", Odometry, get_odometry)
    rospy.Subscriber("/global_plan_proxy", Path, get_path)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    pid.sample_time = 0.01
    while not rospy.is_shutdown():
        if len(path_.poses) > 1:
            for x in path_.poses[::1]:
                go_straight_ahead(x, cmd_vel)
            break
        time.sleep(0.01)


