#!/usr/bin/env python
import time
import rospy
from geometry_msgs.msg import Twist, Point
from nav_msgs.msg import Odometry, Path
from tf import transformations
import math
from simple_pid import PID

pid = PID(1, 0, 0, setpoint=1)
position_x = 0.0
position_y = 0.0
yaw_ = 0.0
path_ = Path()
yaw_precision = math.radians(5.0)
state_ = 0
dist_precision_ = 0.1


def get_odometry(data):
    global position_x, position_y, yaw_
    position_x = data.pose.pose.position.x
    position_y = data.pose.pose.position.y
    quaternion = (data.pose.pose.orientation.x,
                  data.pose.pose.orientation.y,
                  data.pose.pose.orientation.z,
                  data.pose.pose.orientation.w)
    yaw_ = transformations.euler_from_quaternion(quaternion)[2]


def get_path(data):
    global path_
    path_ = data


def yaw_correction(point, vel):
    global yaw_
    desired_yaw = math.atan2(point.pose.position.y - position_y,
                             point.pose.position.x - position_x)
    yaw_error = desired_yaw - yaw_
    #print("desired_yaw " + str(desired_yaw))
    #print("precision " + str(yaw_precision))
    twist_msg = Twist()
    if math.fabs(yaw_error) > yaw_precision:
        twist_msg.angular.z = 0.3 if yaw_error > 0 else -0.3
     #    pid.setpoint = desired_yaw
     #    pid.output_limits = (-0.3, 0.3)
     #    output = pid(yaw_)
     # #   print("output " + str(output))
     #    twist_msg.angular.z = output
    vel.publish(twist_msg)
    if math.fabs(yaw_error) <= yaw_precision:
        print 'yaw error: [%s]' % yaw_error
        change_state(1)


def go_straight_ahead(point, vel):
    global yaw_, yaw_precision, state_
    desired_yaw = math.atan2(point.pose.position.y - position_y,
                             point.pose.position.x - position_x)
    err_yaw = desired_yaw - yaw_
    err_pos = math.sqrt(pow(point.pose.position.y - position_y, 2) +
                        pow(point.pose.position.x - position_x, 2))
    if err_pos > dist_precision_:
        twist_msgs = Twist()
        twist_msgs.linear.x = 0.2
        vel.publish(twist_msgs)
    else:
        print 'position error : [%s]' % err_pos
        change_state(2)

    if math.fabs(err_yaw) > yaw_precision:
        print 'yar error go_St : [%s]' % err_yaw
        change_state(0)


def change_state(state):
    global state_
    state_ = state
    print 'State changed to [%s]' % state_


def done(vel):
    twist_msg = Twist()
    twist_msg.linear.x = 0
    twist_msg.angular.z = 0
    vel.publish(twist_msg)


if __name__ == '__main__':
    rospy.init_node("local_planner", anonymous=True)
    rospy.Subscriber("/outdoor_waypoint_nav/odometry/filtered_map", Odometry, get_odometry)
    rospy.Subscriber("/global_plan_proxy", Path, get_path)
    cmd_vel = rospy.Publisher("/cmd_vel", Twist, queue_size=1)
    pid.sample_time = 0.01
    while not rospy.is_shutdown():
        if len(path_.poses) > 1:
            rate = rospy.Rate(20)
            for x in path_.poses[::1]:
                state_ = 0
                while state_ != 2:
                    if state_ == 0:
                        yaw_correction(x, cmd_vel)
                    elif state_ == 1:
                        go_straight_ahead(x, cmd_vel)
                    elif state_ == 2:
                        done(cmd_vel)
                        state_ = 0
                        #pass
                    else:
                        rospy.logerr('Unknown state!')
                        pass
                    rate.sleep()
            break
        time.sleep(0.01)


