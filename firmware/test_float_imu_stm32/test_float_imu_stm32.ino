void setup() {
  // put your setup code here, to run once:
Serial.begin(57600);
Serial.setTimeout(25);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 1) {
    String floatStr = Serial.readString(); // Read the long string of floats
    float floatArray[27]; // Create an array to store the 27 float values
    int i = 0;
    char *ptr = strtok((char*)floatStr.c_str(), " "); // Parse the long string into individual floats
    while (ptr != NULL && i < 27) {
      floatArray[i] = atof(ptr); // Convert the string to a float and store in the array
      ptr = strtok(NULL, " ");
      i++;
    }
    
    // Print the 27 float values to the serial monitor
    for (int j = 0; j < 27; j++) {
      Serial.print(floatArray[j], 5);
      Serial.print(" ");
    }
    Serial.println();
  }
  delay(0.1);

}
